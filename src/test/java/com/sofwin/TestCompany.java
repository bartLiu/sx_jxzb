package com.sofwin;

import com.sofwin.project.toBenListed.company.pojo.CCompany;
import com.sofwin.project.toBenListed.company.service.CCompanyService;
import org.junit.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestCompany {
    @Autowired
    private CCompanyService companyService;
    @Test
    @Rollback
    public void testSave(){
        CCompany company = new CCompany();
        company.setCompanyName("山西省产权交易中心");
        company.setPassword("123456");
        boolean register = companyService.register(company);
        Assert.assertEquals(true,register);
    }
}

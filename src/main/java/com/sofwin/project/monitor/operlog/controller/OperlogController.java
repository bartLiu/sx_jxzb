package com.sofwin.project.monitor.operlog.controller;

import com.alibaba.fastjson.JSON;
import com.sofwin.common.utils.poi.ExcelUtil;
import com.sofwin.framework.aspectj.lang.annotation.Log;
import com.sofwin.framework.aspectj.lang.enums.BusinessType;
import com.sofwin.framework.web.controller.BaseController;
import com.sofwin.framework.web.domain.AjaxResult;
import com.sofwin.framework.web.page.TableDataInfo;
import com.sofwin.framework.web.service.DictService;
import com.sofwin.project.monitor.operlog.domain.OperLog;
import com.sofwin.project.monitor.operlog.service.IOperLogService;
import com.sofwin.project.system.dict.domain.DictData;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 操作日志记录
 * 
 * @author ruoyi
 */
@Controller
@RequestMapping("/monitor/operlog")
public class OperlogController extends BaseController
{
    private String prefix = "monitor/operlog";

    @Autowired
    private IOperLogService operLogService;

    @Autowired
    private DictService dictService;

    @RequiresPermissions("monitor:operlog:view")
    @GetMapping()
    public String operlog(ModelMap mmap)
    {
        List<DictData> types = dictService.getType("sys_oper_type");
        mmap.put("types",types);
        mmap.put("jsonTypes", JSON.toJSON(types));
        List<DictData> typeStatus = dictService.getType("sys_common_status");
        mmap.put("typeStatus",typeStatus);

        return prefix + "/operlog";
    }

    @RequiresPermissions("monitor:operlog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(OperLog operLog)
    {
        startPage();
        List<OperLog> list = operLogService.selectOperLogList(operLog);
        return getDataTable(list);
    }

    @Log(title = "操作日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("monitor:operlog:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(OperLog operLog)
    {
        List<OperLog> list = operLogService.selectOperLogList(operLog);
        ExcelUtil<OperLog> util = new ExcelUtil<OperLog>(OperLog.class);
        return util.exportExcel(list, "操作日志");
    }

    @RequiresPermissions("monitor:operlog:remove")
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(operLogService.deleteOperLogByIds(ids));
    }

    @RequiresPermissions("monitor:operlog:detail")
    @GetMapping("/detail/{operId}")
    public String detail(@PathVariable("operId") Long operId, ModelMap mmap)
    {
        OperLog operLog = operLogService.selectOperLogById(operId);
        mmap.put("operLog",operLog);
        String label = dictService.getLabel("sys_oper_type", operLog.getBusinessType().toString());
        mmap.put("label",label);
        return prefix + "/detail";
    }
    
    @Log(title = "操作日志", businessType = BusinessType.CLEAN)
    @RequiresPermissions("monitor:operlog:remove")
    @PostMapping("/clean")
    @ResponseBody
    public AjaxResult clean()
    {
        operLogService.cleanOperLog();
        return success();
    }
}

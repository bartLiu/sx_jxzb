package com.sofwin.project.toBenListed.company.service.impl;


import com.sofwin.project.toBenListed.company.mapper.BProvinceMapper;
import com.sofwin.project.toBenListed.company.pojo.BProvince;
import com.sofwin.project.toBenListed.company.service.BProvinceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: ssm-parent
 * @description: 省份业务实现类
 * @author: Mr.lzk
 * @create: 2021-04-24 12:56
 * @Version 1.0
 **/
@Service
public class BProvinceServiceImpl implements BProvinceService {
    @Autowired
    private BProvinceMapper mapper;
    /**
     * @Description: 查询所有省份信息
     * @Author: Mr.lzk
     * @Date: 2021/4/24 12:56
     * @Param:
     * @Return: * @return: 省份集合
     **/
    @Override
    public List<BProvince> selectAllBProvince() {
        return mapper.selectByExample(null);
    }
}

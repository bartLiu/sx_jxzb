package com.sofwin.project.toBenListed.company.dto;


import com.sofwin.project.toBenListed.company.pojo.BCity;
import com.sofwin.project.toBenListed.company.pojo.BProvince;
import com.sofwin.project.toBenListed.company.pojo.CCompany;
import com.sofwin.project.toBenListed.company.pojo.DIposecond;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: ssm-parent
 * @description: 客户表扩展类
 * @author: Mr.lzk
 * @create: 2021-04-25 15:32
 * @Version 1.0
 **/
@Data
public class CCompanyDto extends CCompany implements Serializable {
    // 行业名称
    private DIposecond ss;
    // 省份名称
    private BProvince pp;
    // 城市名称
    private BCity cc;



}

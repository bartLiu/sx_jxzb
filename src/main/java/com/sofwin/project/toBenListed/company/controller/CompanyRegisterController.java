package com.sofwin.project.toBenListed.company.controller;


import com.sofwin.framework.web.controller.BaseController;
import com.sofwin.project.toBenListed.company.pojo.BCity;
import com.sofwin.project.toBenListed.company.pojo.BProvince;
import com.sofwin.project.toBenListed.company.pojo.CCompany;
import com.sofwin.project.toBenListed.company.pojo.DIposecond;
import com.sofwin.project.toBenListed.company.service.BCityService;
import com.sofwin.project.toBenListed.company.service.BProvinceService;
import com.sofwin.project.toBenListed.company.service.CCompanyService;
import com.sofwin.project.toBenListed.company.service.DIposecondService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: ssm-parent
 * @description: 用户注册
 * @author: Mr.lzk
 * @create: 2021-04-23 18:07
 * @Version 1.0
 **/
@Controller
public class CompanyRegisterController extends BaseController {
    @Autowired
    private DIposecondService dIposecondService;
    @Autowired
    private BProvinceService bProvinceService;
    @Autowired
    private BCityService bCityService;
    @Autowired
    private CCompanyService cCompanyService;
    /**
     * @Description: 跳转到第一个注册页
     * @Author: Mr.lzk
     * @Date: 2021/4/23 20:26
     * @Param:
     * @Return: * @return: java.lang.String
     **/
    @GetMapping("toRegister")
    public String toRegister(){
        return "register/userregister";
    }

    /**
     * @Description: 查询所有二级行业信息及所有省份信息跳转到第二个注册页
     * @Author: Mr.lzk
     * @Date: 2021/4/23 20:27
     * @Param: * @param company:
     * @param model:
     * @Return: * @return: java.lang.String
     **/
    @PostMapping("toNextRegister")
    public String toNextRegister(CCompany company, Model model){
        List<DIposecond> dIposeconds = dIposecondService.selectAllDiposecond();
        List<BProvince> bProvinces = bProvinceService.selectAllBProvince();
        model.addAttribute("company",company);
        // 二级行业
        model.addAttribute("dIposeconds",dIposeconds);
        // 省份
        model.addAttribute("bProvinces",bProvinces);
        return "register/userregister1";
    }

    /**
     * @Description: 根据省编号查询所有城市
     * @Author: Mr.lzk
     * @Date: 2021/4/24 17:45
     * @Param: * @param provinceId:
     * @Return: * @return: 城市集合
     **/
    @RequestMapping("queryCity")
    @ResponseBody
    public List queryCity(String provinceId){
        List<BCity> bCities = bCityService.queryCity(provinceId);
        return bCities;
    }


    /**
     * @Description: 存入注册数据
     * @Author: Mr.lzk
     * @Date: 2021/4/24 17:45
     * @Param: * @param company:
     * @Return: * @return: 注册状态（是否成功）
     **/
    @RequestMapping("cregister")
    @ResponseBody
    public Map register(CCompany company) {
        Map map = new HashMap();
        boolean status = cCompanyService.register(company);
        map.put("status",status);
        return map;
    }


    /**
    * @Author abiao
    * @Description  公司名称查重
    * @Date 2021/4/29 下午 2:35
    * @param companyName 公司名称
    * @return Map json
    */
    @GetMapping("checkCompany")
    @ResponseBody
    public Map check(String companyName){
        System.out.println(companyName);
        Map map = new HashMap();
        boolean status = cCompanyService.check(companyName);
        map.put("status",status);
        return map;
    }
}

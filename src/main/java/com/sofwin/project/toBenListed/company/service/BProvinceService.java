package com.sofwin.project.toBenListed.company.service;


import com.sofwin.project.toBenListed.company.pojo.BProvince;

import java.util.List;

public interface BProvinceService {
    /**
     * @Description: 查询所有的省份信息
     * @Author: Mr.lzk
     * @Date: 2021/4/24 12:54
     * @Param:
     * @Return: * @return: 省份集合
     **/
    List<BProvince> selectAllBProvince();
}

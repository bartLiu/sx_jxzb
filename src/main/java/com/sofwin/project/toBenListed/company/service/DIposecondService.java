package com.sofwin.project.toBenListed.company.service;


import com.sofwin.project.toBenListed.company.pojo.DIposecond;
import org.springframework.stereotype.Service;

import java.util.List;


public interface DIposecondService {
    /**
     * @Description: 查询所有的二级行业
     * @Author: Mr.lzk
     * @Date: 2021/4/23 19:57
     * @Param:
     * @Return: * @return: 二级行业集合
     **/
    List<DIposecond> selectAllDiposecond();
}

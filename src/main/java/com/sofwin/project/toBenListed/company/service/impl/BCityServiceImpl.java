package com.sofwin.project.toBenListed.company.service.impl;


import com.sofwin.project.toBenListed.company.mapper.BCityMapper;
import com.sofwin.project.toBenListed.company.pojo.BCity;
import com.sofwin.project.toBenListed.company.pojo.BCityExample;
import com.sofwin.project.toBenListed.company.service.BCityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: ssm-parent
 * @description: 城市业务实现类
 * @author: Mr.lzk
 * @create: 2021-04-24 15:04
 * @Version 1.0
 **/
@Service
public class BCityServiceImpl implements BCityService {
    @Autowired
    private BCityMapper mapper;
    /**
     * @Description: 根据省编号查询所有的城市
     * @Author: Mr.lzk
     * @Date: 2021/4/24 15:05
     * @Param: * @param provinceId:
     * @Return: * @return: 城市集合
     **/
    @Override
    public List<BCity> queryCity(String provinceId) {
        BCityExample example = new BCityExample();
        BCityExample.Criteria criteria = example.createCriteria();
        criteria.andParentIdEqualTo(provinceId);
        return mapper.selectByExample(example);
    }
}

package com.sofwin.project.toBenListed.company.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import com.google.inject.internal.asm.$ClassReader;
import com.sofwin.project.toBenListed.company.dto.CCompanyDto;
import com.sofwin.project.toBenListed.company.mapper.CCompanyMapper;
import com.sofwin.project.toBenListed.company.pojo.CCompany;
import com.sofwin.project.toBenListed.company.pojo.CCompanyExample;
import com.sofwin.project.toBenListed.company.service.CCompanyService;
import io.swagger.annotations.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: ssm-parent
 * @description: 客户业务实现类
 * @author: Mr.lzk
 * @create: 2021-04-24 17:52
 * @Version 1.0
 **/
@Service
public class CCompanyServiceImpl implements CCompanyService {
    @Autowired
    private CCompanyMapper mapper;
    /**
     * @Description: 存入注册信息
     * @Author: Mr.lzk
     * @Date: 2021/4/24 17:53
     * @Param: * @param company:
     * @Return: * @return: 注册状态
     **/
    @Override
    public boolean register(CCompany company) {
        return mapper.insertSelective(company) == 1 ? true : false;
    }

    /**
     * @Description: 分页查询所有未审核的公司
     * @Author: Mr.lzk
     * @Date: 2021/4/25 14:56
     * @Param: * @param company: 筛选条件
     * @param pageNumber: 当前页码
     * @param pageSize: 每页条数
     * @Return: * @return: com.github.pagehelper.PageInfo<com.sofwin.pojo.CCompany>
     **/
    @Override
    public PageInfo<CCompanyDto> selectCompanyListByPage(CCompany company, Integer pageNumber, Integer pageSize) {
        PageHelper.startPage(pageNumber,pageSize);
        List<CCompanyDto> cCompanies = mapper.selectCompanys(company);
        return new PageInfo<>(cCompanies);
    }

    /**
     * @Description: 更改审核状态（通过）
     * @Author: Mr.lzk
     * @Date: 2021/4/26 15:09
     * @Param: * @param id:
     * @Return: * @return: boolean
     **/
    @Override
    public boolean pass(Integer id) {
        return mapper.passFlag(id) == 1 ? true : false;
    }

    /**
     * @Description: 更改审核状态（未通过）
     * @Author: Mr.lzk
     * @Date: 2021/4/26 15:10
     * @Param: * @param id:
     * @Return: * @return: boolean
     **/
    @Override
    public boolean noPass(Integer id) {
        return mapper.noPassFlag(id) == 1 ? true : false;
    }

    @Override
    public CCompany selectCompanyByName(String companyName, String pwd) {
        CCompanyExample example = new CCompanyExample();
        CCompanyExample.Criteria criteria = example.createCriteria();
        criteria.andCompanyNameEqualTo(companyName);
        criteria.andPasswordEqualTo(pwd);
        List<CCompany> cCompanies = mapper.selectByExample(example);
        if(cCompanies==null || cCompanies.size()==0){
            return null;
        }else{
            return cCompanies.get(0);
        }
    }

    @Override
    public boolean check(String companyName) {
        CCompanyExample example = new CCompanyExample();
        CCompanyExample.Criteria criteria = example.createCriteria();
        criteria.andCompanyNameEqualTo(companyName);
        return mapper.countByExample(example) != 0?false:true;
    }

}

package com.sofwin.project.toBenListed.company.service.impl;

import com.sofwin.project.toBenListed.company.mapper.DIposecondMapper;
import com.sofwin.project.toBenListed.company.pojo.DIposecond;
import com.sofwin.project.toBenListed.company.service.DIposecondService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

    /**
     * @program: ssm-parent
     * @description: 二级行业业务实现类
     * @author: Mr.lzk
     * @create: 2021-04-23 19:58
     * @Version 1.0
     **/
    @Service
    public class DIposecondServiceImpl implements DIposecondService {
    @Autowired
    private DIposecondMapper mapper;

    /**
     * @Description: 查询所有的二级行业
     * @Author: Mr.lzk
     * @Date: 2021/4/23 20:24
     * @Param:
     * @Return: * @return: 二级行业集合
     **/
    @Override
    public List<DIposecond> selectAllDiposecond() {
        return mapper.selectByExample(null);
    }
}

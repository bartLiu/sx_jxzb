package com.sofwin.project.toBenListed.company.service;



import com.sofwin.project.toBenListed.company.pojo.BCity;

import java.util.List;

public interface BCityService {
    /**
     * @Description: 根据省编号查询所有城市
     * @Author: Mr.lzk
     * @Date: 2021/4/24 15:04
     * @Param: * @param provinceId:
     * @Return: * @return: java.util.List<com.sofwin.pojo.BCity>
     **/
    List<BCity> queryCity(String provinceId);
}

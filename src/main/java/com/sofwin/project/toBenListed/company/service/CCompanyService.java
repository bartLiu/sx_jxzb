package com.sofwin.project.toBenListed.company.service;

import com.github.pagehelper.PageInfo;
import com.sofwin.project.toBenListed.company.dto.CCompanyDto;
import com.sofwin.project.toBenListed.company.pojo.CCompany;


public interface CCompanyService {
    /**
     * @Description: 存入注册信息
     * @Author: Mr.lzk
     * @Date: 2021/4/24 17:51
     * @Param: * @param company: 需要存入的信息
     * @Return: * @return: 注册状态
     **/
     boolean register(CCompany company);

    /**
     * @Description: 分页查询所有未审核的公司
     * @Author: Mr.lzk
     * @Date: 2021/4/25 14:54
     * @Param: * @param company: 筛选条件
     * @param pageNumber: 当前页码
     * @param pageSize: 每页条数
     * @Return: * @return: com.github.pagehelper.PageInfo<com.sofwin.pojo.CCompany>
     **/
     PageInfo<CCompanyDto> selectCompanyListByPage(CCompany company, Integer pageNumber, Integer pageSize);

    /**
     * @Description: 更改审核状态（通过）
     * @Author: Mr.lzk
     * @Date: 2021/4/26 11:03
     * @Param: * @param id:
     * @Return: * @return: boolean
     **/
     boolean pass(Integer id);

    /**
     * @Description: 更改审核状态（未通过）
     * @Author: Mr.lzk
     * @Date: 2021/4/26 15:09
     * @Param: * @param id:
     * @Return: * @return: boolean
     **/
    boolean noPass(Integer id);

    /**
     * 根据名称查询企业信息
     * @param companyName
     * @return
     */
    CCompany selectCompanyByName(String companyName,String pwd);

    /**
    * @Author abiao
    * @Description 检查公司名称是否重复
    * @Date 2021/4/29 下午 2:32
    * @param companyName 公司名称
    * @return boolean true 不重复|false 有重复
    */
    boolean check(String companyName);
}

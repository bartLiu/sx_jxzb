package com.sofwin.project.system.dict.controller;

import com.alibaba.fastjson.JSON;
import com.sofwin.common.utils.poi.ExcelUtil;
import com.sofwin.framework.aspectj.lang.annotation.Log;
import com.sofwin.framework.aspectj.lang.enums.BusinessType;
import com.sofwin.framework.web.controller.BaseController;
import com.sofwin.framework.web.domain.AjaxResult;
import com.sofwin.framework.web.page.TableDataInfo;
import com.sofwin.framework.web.service.DictService;
import com.sofwin.project.system.dict.domain.DictData;
import com.sofwin.project.system.dict.service.IDictDataService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 数据字典信息
 * 
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/dict/data")
public class DictDataController extends BaseController
{
    private String prefix = "system/dict/data";

    @Autowired
    private IDictDataService dictDataService;

    @Autowired
    private DictService dictService;

    @RequiresPermissions("system:dict:view")
    @GetMapping()
    public String dictData(ModelMap mmap)
    {
        List<DictData> types = dictService.getType("sys_normal_disable");
        mmap.addAttribute("types",types);
        mmap.addAttribute("keys", JSON.toJSON(types));
        return prefix + "/data";
    }

    @PostMapping("/list")
    @RequiresPermissions("system:dict:list")
    @ResponseBody
    public TableDataInfo list(DictData dictData)
    {
        startPage();
        List<DictData> list = dictDataService.selectDictDataList(dictData);

        return getDataTable(list);
    }

    @Log(title = "字典数据", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:dict:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(DictData dictData)
    {
        List<DictData> list = dictDataService.selectDictDataList(dictData);
        ExcelUtil<DictData> util = new ExcelUtil<DictData>(DictData.class);
        return util.exportExcel(list, "字典数据");
    }

    /**
     * 新增字典类型
     */
    @GetMapping("/add/{dictType}")
    public String add(@PathVariable("dictType") String dictType, ModelMap mmap)
    {
        mmap.put("dictType", dictType);
        List<DictData> types = dictService.getType("sys_normal_disable");
        mmap.addAttribute("types",types);
        List<DictData> type_yes = dictService.getType("sys_yes_no");
        mmap.addAttribute("typeYes",type_yes);

        return prefix + "/add";
    }

    /**
     * 新增保存字典类型
     */
    @Log(title = "字典数据", businessType = BusinessType.INSERT)
    @RequiresPermissions("system:dict:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated DictData dict)
    {
        return toAjax(dictDataService.insertDictData(dict));
    }

    /**
     * 修改字典类型
     */
    @GetMapping("/edit/{dictCode}")
    public String edit(@PathVariable("dictCode") Long dictCode, ModelMap mmap)
    {
        mmap.put("dict", dictDataService.selectDictDataById(dictCode));
        List<DictData> types = dictService.getType("sys_normal_disable");
        mmap.addAttribute("types",types);
        List<DictData> type_yes = dictService.getType("sys_yes_no");
        mmap.addAttribute("typeYes",type_yes);
        return prefix + "/edit";
    }

    /**
     * 修改保存字典类型
     */
    @Log(title = "字典数据", businessType = BusinessType.UPDATE)
    @RequiresPermissions("system:dict:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated DictData dict)
    {
        return toAjax(dictDataService.updateDictData(dict));
    }

    @Log(title = "字典数据", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:dict:remove")
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(dictDataService.deleteDictDataByIds(ids));
    }
}

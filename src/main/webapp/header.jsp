<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:43 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="shortcut icon" href="favicon.ico">
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/font-awesome.min.css" rel="stylesheet"/>
<!-- bootstrap-table 表格插件样式 -->
<link href="ajax/libs/bootstrap-table/bootstrap-table.min.css?v=20210202" rel="stylesheet"/>
<link href="css/animate.css" rel="stylesheet"/>
<link href="css/style.css?v=20200903" rel="stylesheet"/>
<link href="ruoyi/css/ry-ui.css?v=4.6.1" rel="stylesheet"/>
<a id="scroll-up" href="#" class="btn btn-sm display"><i class="fa fa-angle-double-up"></i></a>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- bootstrap-table 表格插件 -->
<script src="ajax/libs/bootstrap-table/bootstrap-table.min.js?v=20210202"></script>
<script src="ajax/libs/bootstrap-table/locale/bootstrap-table-zh-CN.min.js?v=20210202"></script>
<script src="ajax/libs/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js?v=20210202"></script>
<!-- jquery-validate 表单验证插件 -->
<script src="ajax/libs/validate/jquery.validate.min.js"></script>
<script src="ajax/libs/validate/messages_zh.min.js"></script>
<script src="ajax/libs/validate/jquery.validate.extend.js"></script>
<!-- jquery-validate 表单树插件 -->
<script src="ajax/libs/bootstrap-treetable/bootstrap-treetable.js"></script>
<!-- 遮罩层 -->
<script src="ajax/libs/blockUI/jquery.blockUI.js"></script>
<script src="ajax/libs/iCheck/icheck.min.js"></script>
<script src="ajax/libs/layer/layer.min.js"></script>
<script src="ajax/libs/layui/layui.js"></script>
<script src="ruoyi/js/common.js?v=4.6.1"></script>
<script src="ruoyi/js/ry-ui.js?v=4.6.1"></script>

<%--
  Created by IntelliJ IDEA.
  User: ccvv
  Date: 2021/4/23
  Time: 17:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% String path = request.getContextPath()+"/"; %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head lang="en">
    <base href="<%=path%>">
    <meta charset="UTF-8">
    <title>注册</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link href="css/bootstrap.min.css"  rel="stylesheet"/>
    <link href="css/font-awesome.min.css" rel="stylesheet"/>
    <link href="css/style.css"  rel="stylesheet"/>
    <link href="css/login.min.css"  rel="stylesheet"/>
    <link href="ruoyi/css/ry-ui.css" rel="stylesheet"/>
    <!-- 360浏览器急速模式 -->
    <meta name="renderer" content="webkit">
    <!-- 避免IE使用兼容模式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="favicon.ico" />
    <style type="text/css">
        label.error { position:inherit;  }
        .control-label{
            margin-top: 15px;
        }
    </style>
    <![endif]-->
</head>

<body class="signin">


    <div class="signinpanel">

            <div class="col-sm-8" style="margin-left: 400px; margin-top: -40px;">
                <form class="form-horizontal"  id="form1" autocomplete="off" style="color: black" >
                    <!--<div class="layui-form-item">
                        <label for="username" class="layui-form-label" style="width: 130px">
                            <span class="x-red">*</span>企业名称
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="username" name="username" value="山西软赢科技" required="" lay-verify="required"
                                   autocomplete="off" class="layui-input">
                        </div>
                    </div>-->
                    <input type="hidden" name="companyName" value="${company.companyName}">
                    <input type="hidden" name="email" value="${company.email}">
                    <input type="hidden" name="person" value="${company.person}">
                    <input type="hidden" name="password" value="${company.password}">
                    <input type="hidden" name="invitecode" value="${company.invitecode}">

                    <%--<div class="layui-form-item">
                        <label class="layui-form-label" style="width: 130px">
                            <span class="x-red">*</span>所属行业
                        </label>
                        <div class="layui-input-inline">
                            <select name="industry" lay-verify="required">
                                <option value="">选择企业所属行业</option>
                                <c:forEach items="${dIposeconds}" var="dIposecond">
                                    <option value="${dIposecond.secondCode}">${dIposecond.secondName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>--%>
                    <div class="form-group">
                        <label for="industry" class="col-sm-5 control-label"><span style="color: red">*</span>所属行业</label>
                        <div class="col-sm-7">
                            <select class="form-control" id="industry" name="industry">
                                <option value="" style="color: grey">选择企业所属行业</option>
                                <c:forEach items="${dIposeconds}" var="dIposecond">
                                    <option value="${dIposecond.secondCode}">${dIposecond.secondName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <%--<div class="layui-form-item">
                        <label for="L_email" class="layui-form-label" style="width: 130px">
                            <span class="x-red">*</span>企业类型
                        </label>
                        <div class="layui-input-inline">
                            <select name="type" lay-verify="required">
                                <option value="">选择企业类型</option>
                                <option value="1">有限责任公司</option>
                                <option value="2">股份有限公司</option>
                            </select>
                        </div>
                    </div>--%>
                    <div class="form-group">
                        <label for="type" class="col-sm-5 control-label"><span style="color: red">*</span>企业类型</label>
                        <div class="col-sm-7">
                        <select class="form-control" id="type" name="type">
                            <option value="" >选择企业类型</option>
                            <option value="1">有限责任公司</option>
                            <option value="2">股份有限公司</option>
                        </select>
                        </div>
                    </div>
                    <%--<div class="layui-form-item">
                        <label for="L_email" class="layui-form-label" style="width: 130px">
                            <span class="x-red">*</span>所在地
                        </label>
                        <div class="layui-input-inline">
                            <select name="province" lay-verify="required" lay-filter="province" id="province">
                                <option value="">选择省份</option>
                                <c:forEach items="${bProvinces}" var="bProvince">
                                    <option value="${bProvince.provinceId}">${bProvince.province}</option>
                                </c:forEach>
                            </select>
                            <select name="city" lay-verify="required" id="city" lay-filter="city">

                            </select>
                        </div>

                    </div>--%>
                    <div class="form-group">
                        <label for="province" class="col-sm-5 control-label"><span style="color: red">*</span>所在地</label>
                        <div class="col-sm-7">
                        <select class="form-control" id="province" name="province">
                            <option value="" >选择省份</option>
                            <c:forEach items="${bProvinces}" var="bProvince">
                                <option value="${bProvince.provinceId}">${bProvince.province}</option>
                            </c:forEach>
                        </select>
                        <select class="form-control" id="city" name="city"   style="margin-top: 0px;">
                            <option value="">选择城市</option>
                        </select>
                        </div>
                    </div>

                    <%--<div class="form-group">
                        <label for="L_email" class="layui-form-label" style="width: 130px">
                            <span class="x-red">*</span>详细地址
                        </label>
                        <div class="layui-input-inline" >
                            <input class="form-control" type="text" placeholder="Default input" name="address">
                        </div>

                    </div>--%>
                    <div class="form-group">
                        <label  class="col-sm-5 control-label"><span style="color: red">*</span>详细地址</label>
                        <div class="col-sm-7">
                        <input type="text" name="address" class="form-control">
                        </div>
                    </div>
                    <%--<div class="layui-form-item">
                        <label for="L_email" class="layui-form-label" style="width: 130px">
                            <!-- <span class="x-red">*</span>-->企业网址
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="L_email" name="website"
                                   autocomplete="off" class="layui-input" lay-verify="url">
                        </div>
                    </div>--%>

                    <div class="form-group">
                        <label  class="col-sm-5 control-label">企业网址</label>
                        <div class="col-sm-7">
                        <input type="url" name="website" class="form-control">
                        </div>
                    </div>


                    <input type="hidden" value="0" name="flag">
                    <%--<div class="layui-form-item">
                        <label for="L_email" class="layui-form-label" style="width: 130px">

                        </label>
                        <div class="layui-input-inline">
                            <button  class="layui-btn" lay-filter="add" lay-submit="">
                                注册
                            </button>
                        </div>
                    </div>--%>
                    <div class="form-group">
                        <label class="col-sm-6 control-label" ></label>
                        <div class="col-sm-6">
                                <button type="submit" class="btn btn-primary" id="btn">注册</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <!-- <div class="login-links">
             <label for="remember-me"><input id="remember-me" type="checkbox">记住密码</label>
             <a href="#" class="am-fr">忘记密码</a>
               <a href="register.html" class="zcnext am-fr am-btn-default">注册</a>
             <br />
         </div>
         <div class="am-cf">
             <a href="./index1.html"><input type="submit" name="" value="登 录" class="am-btn am-btn-primary am-btn-sm"></a>
         </div>-->
</div>

    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <!-- 验证插件 -->
    <script src="ajax/libs/validate/jquery.validate.min.js" type="text/javascript"></script>
    <script src="ajax/libs/validate/messages_zh.min.js" type="text/javascript"></script>
    <script src="ajax/libs/layer/layer.min.js" type="text/javascript"></script>
    <script src="ajax/libs/blockUI/jquery.blockUI.js"type="text/javascript"></script>
    <script src="ruoyi/js/ry-ui.js" type="text/javascript"></script>
<script>
        $("#province").change(function () {
            $("#city").empty();
            var provinceId = $("#province").val();
            $.get('queryCity?provinceId='+provinceId,function (ret) {

                $("#city").append(
                    "<option value=''>"+"选择城市"+"</option>"
                )
                for(let i = 0; i < ret.length; i ++){
                    $("#city").append(
                        "<option value='"+ret[i].cityId+"'>"+ret[i].city+"</option>"
                    )
                    // document.getElementById("city").options.add(new Option(ret[i].cityId,ret[i].city));
                }

            })
        })
        // 点击注册，验证表单
        $("#btn").click(function () {
            validateRule();
        });
        function validateRule() {
            var icon = "<i class='fa fa-times-circle'></i> ";
            $("#form1").validate({
                submitHandler:function () {
                        var company = $("#form1").serialize()+"&registertime="+new Date().toLocaleString();
                        $.ajax({
                            url:'cregister',
                            dataType:'json',
                            type:'post',
                            data:company,
                            success:function (ret) {
                                /*alert(JSON.stringify(ret));*/
                                if(ret.status){
                                    layer.alert("注册成功,请等待管理员审核".fontcolor("black"), {
                                            anim: 6,
                                            icon: 6
                                        },
                                        function() {
                                            //关闭当前frame
                                            window.location.href='login.jsp';
                                        });
                                }else {
                                    layer.msg("操作失败").fontcolor("black");
                                }
                            }
                        })

                },
                rules: {
                    address: "required",
                    province: "required",
                    city: "required",
                    industry: "required",
                    type: "required"
                },
                messages: {
                    address: icon+"请输入详细地址",
                    province: icon+"请选择省份",
                    city: icon+"请选择城市",
                    industry: icon+"请选择行业",
                    type: icon+"请选择公司类型"
                }
            })
        }

        //监听提交
        /*$("#btn").c
            function(data) {
                var len = $("#form1")[0].elements.length;
                for (let i = 6; i < len-2; i ++) {
                    if (i == 7 || i == 9 || i == 11) {
                        continue;
                    }
                    if ($("#form1")[0].elements[i].value == "") {
                        return false;
                    }
                }
                })*/

                //发异步，把数据提交给php


</script>
<%--<script>var _hmt = _hmt || []; (function() {
    var hm = document.createElement("script");
    hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(hm, s);
})();</script>--%>
</body>

</html>

<%--
  Created by IntelliJ IDEA.
  User: ccvv
  Date: 2021/4/25
  Time: 11:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% String path = request.getContextPath()+"/"; %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    <base href="<%=path%>">
    <meta charset="UTF-8">
    <title>注册码管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="layui/css/font.css">
    <link rel="stylesheet" href="layui/css/xadmin.css">
    <script src="layui/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="layui/js/xadmin.js"></script>
    <script type="text/javascript" src="layui/js/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body ">
                    <form class="layui-form layui-col-space5" action="/registercode/data" method="get" id="form">
                        <input type="hidden" name="pageNumber" value="${page.pageNum}" id="pageNumber" />
                        <input type="hidden" name="pageSize" value="${page.pageSize}" />
                        <div class="layui-inline layui-show-xs-block">
                            <input type="text" name="companyName" value="${company.companyName}" placeholder="请输入关键字" autocomplete="off" class="layui-input">
                        </div>
                       <%-- <div class="layui-inline layui-show-xs-block">
                            <input class="layui-input"  autocomplete="off" placeholder="开始日" name="start" id="start">
                        </div>
                        <div class="layui-inline layui-show-xs-block">
                            <input class="layui-input"  autocomplete="off" placeholder="截止日" name="end" id="end">
                        </div>--%>
                        <div class="layui-inline layui-show-xs-block">
                            <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                            <!--  <button type="button" onclick="xadmin.open('新增企业','./conpany-add.html',500,500)"
                                      class="layui-btn">新增企业</button>-->
                        </div>
                    </form>
                </div>
                <div class="layui-card-body ">
                    <table class="layui-table layui-form">
                        <thead>
                        <tr align="center">
                            <td>序号</td>
                            <td>用户名</td>
                            <td>企业邮箱</td>
                            <td>联系人</td>
                            <td>所属行业</td>
                            <td>企业类型</td>
                            <td>所在地</td>
                            <!-- <td>详细地址</td>-->
                            <td>操作</td>
                        </thead>
                        <tbody>
                        <c:choose>
                            <c:when test="${fn:length(data)==0}">
                                <tr>
                                    <td  colspan="9" align="center">暂无数据</td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach items="${data}" var="comp" varStatus="status">
                                    <tr align="center">
                                        <td>${status.count}</td>
                                        <td>${comp.companyName}</td>
                                        <td>${comp.email}</td>
                                        <td>
                                                ${comp.person}
                                        </td>
                                        <td>${comp.ss.secondName}</td>
                                        <td>
                                            <c:if test="${comp.type == 1}" >
                                                有限责任公司
                                            </c:if>
                                            <c:if test="${comp.type == 2}">
                                                股份有限公司
                                            </c:if>
                                        </td>
                                        <td>${comp.pp.province}${comp.cc.city}</td>
                                        <!--  <td>山西省晋中市榆次区**</td>-->
                                        <td>
                                            <button
                                                    class="layui-btn" data="${comp.id}">待审核</button>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>

                        </tbody>
                    </table>
                </div>
                <div class="layui-card-body ">
                    <div class="page" id="page"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    layui.use(['laydate','form','laypage','layer','jquery'], function(){
        var laydate = layui.laydate;
        var form = layui.form;
        var laypage = layui.laypage;
        var layer = layui.layer;
        $ = layui.jquery;
        laypage.render({
            elem:'page',
            count:parseInt('${page.total}'),
            limit:parseInt('${page.pageSize}'),
            limits:[10,20,30],
            curr:${page.pageNum},
            layout:['prev', 'page', 'next','skip'],
            jump:function(obj,first){
                if(!first){
                    // 手动在切换页码
                    $("#pageNumber").val(obj.curr);
                    $("#form").submit();
                }
            }
        })






        /*//执行一个laydate实例
        laydate.render({
            elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
            elem: '#end' //指定元素
        });*/
    });

    /*用户-停用*/
    $(".layui-btn").click(function () {
        let id = $(this).attr("data");
        layer.confirm('是否通过审核？',{
            btn:['通过','不通过','取消'],
            icon:3
        },function (index) {
            $.ajax({
                url:'registercode/pass?id='+id,
                type:'get',
                dataType:'json',
                success:function (ret) {
                    if(ret.status){
                        layer.msg('操作成功!',{
                            offset:'t',
                            time:2000,
                            anim:6
                        },function (){
                            if(${page.size == 1}){
                                var pageNumber = $("#pageNumber").val() - 1;
                                $("#pageNumber").val(pageNumber);
                            }
                            $("#form").submit();
                        })
                    }
                }
            })
        },function (index) {
            $.ajax({
                url:'registercode/noPass?id='+id,
                type:'get',
                dataType:'json',
                success:function (ret){
                    if(ret.status){
                        layer.msg('操作成功!',{
                            offset:'t',
                            time:2000,
                            anim:6
                        },function (){
                            if(${page.size == 1}){
                                var pageNumber = $("#pageNumber").val() - 1;
                                $("#pageNumber").val(pageNumber);
                            }
                            $("#form").submit();
                        })
                    }
                }
            })
        });



    });


    /*用户-删除*/
    function member_del(obj,id){
        layer.confirm('确认要删除吗？',function(index){
            //发异步删除数据
            $(obj).parents("tr").remove();
            layer.msg('已删除!',{icon:1,time:1000});
        });
    }



    function delAll (argument) {

        var data = tableCheck.getData();

        layer.confirm('确认要删除吗？'+data,function(index){
            //捉到所有被选中的，发异步进行删除
            layer.msg('删除成功', {icon: 1});
            $(".layui-form-checked").not('.header').parents('tr').remove();
        });
    }
</script>
<script>var _hmt = _hmt || []; (function() {
    var hm = document.createElement("script");
    hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(hm, s);
})();</script>
</html>
<%--
  Created by IntelliJ IDEA.
  User: ccvv
  Date: 2021/4/23
  Time: 15:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% String path = request.getContextPath()+"/"; %>
<!DOCTYPE html>
<html>

<head lang="en">
    <base href="<%=path%>">
    <meta charset="UTF-8">
    <title>注册</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link href="css/bootstrap.min.css"  rel="stylesheet"/>
    <link href="css/font-awesome.min.css" rel="stylesheet"/>
    <link href="css/style.css"  rel="stylesheet"/>
    <link href="css/login.min.css"  rel="stylesheet"/>
    <link href="ruoyi/css/ry-ui.css" rel="stylesheet"/>
    <!-- 360浏览器急速模式 -->
    <meta name="renderer" content="webkit">
    <!-- 避免IE使用兼容模式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="favicon.ico" />
    <style type="text/css">label.error { position:inherit;  }</style>
</head>

<body class="signin">
<div class="container">
    <div class="row">
        <div class="col-sm-5">
            <div class="signin-info">
                <div class="logopanel m-b">
                    <h1></h1>
                </div>
                <div class="m-b"></div>

            </div>
        </div>
        <div class="col-sm-7" style="margin-top: 200px" >
                <form  id="form1" class="form-horizontal" style="color: black" action="/toNextRegister" method="post">
                    <div class="form-group">
                        <label for="companyName" class="col-sm-5 control-label" style="color: black;"><span style="color: red;">*</span>企业名称</label>
                        <div class="col-sm-7" style="position: relative" >
                            <input type="text"  class="form-control" name="companyName" id="companyName" placeholder="请输入企业名称">
                            <span style="color: red; font-size: 12px; position: absolute;top: 33px;left: 15px" id="checkN"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-5 control-label" style="color: black;"><span style="color: red;">*</span>企业邮箱</label>
                        <div class="col-sm-7">
                            <input type="email"  class="form-control" name="email" id="email" placeholder="请输入企业邮箱">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="person" class="col-sm-5 control-label" style="color: black;"><span style="color: red;">*</span>联系人</label>
                        <div class="col-sm-7">
                            <input type="text"  class="form-control" name="person" id="person" placeholder="请输入联系人">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-5 control-label" style="color: black;"><span style="color: red;">*</span>密码</label>
                        <div class="col-sm-7">
                            <input type="password"  class="form-control" name="password" id="password" placeholder="请输入密码">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirmPassword" class="col-sm-5 control-label" style="color: black;"><span style="color: red;">*</span>确认密码</label>
                        <div class="col-sm-7">
                            <input type="password"  class="form-control" name="confirmPassword" id="confirmPassword" placeholder="请重新输入密码">
                            <span style="color: red; font-size: 12px; position: absolute;" id="pwd-msg"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="invitecode" class="col-sm-5 control-label" style="color: black;"><span style="color: red;">*</span>邀请码</label>
                        <div class="col-sm-7">
                            <input type="text"  class="form-control" name="invitecode" id="invitecode" placeholder="请输入邀请码">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-5 control-label" >
                        </label>
                        <div class="col-sm-7">
                                <button type="submit" class="btn btn-primary" style="margin-left: 40px"  >
                                下一步
                            </button>
                            <button type="submit" class="btn btn-success" style="margin-left: 70px">
                                保存
                            </button>
                        </div>
                    </div>
                </form>
            </div>
    </div>
</div>
</div>
</body>
<!-- 全局js -->
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<!-- 验证插件 -->
<script src="ajax/libs/validate/jquery.validate.min.js" type="text/javascript"></script>
<script src="ajax/libs/validate/messages_zh.min.js" type="text/javascript"></script>
<script src="ajax/libs/layer/layer.min.js" type="text/javascript"></script>
<script src="ajax/libs/blockUI/jquery.blockUI.js"type="text/javascript"></script>
<script src="ruoyi/js/ry-ui.js" type="text/javascript"></script>
<script>

    $(function() {
        validateRule();
    });
    // 验证表单
    function validateRule() {
        var icon = "<i class='fa fa-times-circle'></i> ";
        $("#form1").validate({
            rules: {
                companyName: {
                    required: true,
                    checkName:true,
                },
                email:{
                    required:true,
                    email:true
                },
                person:{
                    required:true,
                    rangelength:[2,15]
                },
                password: {
                    required: true,
                    checkPass:true,
                },
                confirmPassword:{
                    equalTo:'#password'
                },
                invitecode:{
                    required:true,
                    rangelength:[6,8]
                }
            },
            messages: {
                companyName: {
                    required: icon + "请输入您的企业名称",
                },
                email:{
                  required:icon + "邮箱不能为空",
                  email:icon + "请检查邮箱格式",
                },
                person:{
                    required:icon + "联系人不能为空",
                    rangelength:icon + "长度在2~15之间",
                },
                password: {
                    required: icon + "请输入您的密码",
                    rangelength:icon + "密码长度在6~15之间",
                },
                confirmPassword:{
                    equalTo: icon + "两次输入密码不一致，请重新输入",
                },
                invitecode:{
                    required:icon + "邀请码不能为空",
                    rangelength:icon + "长度在6~8之间",
                }
            }
        })
    }
    // 自定义正则表达式验证方法
    // 公司名称正则表达式
    $.validator.addMethod("checkName",function(value,element,params){
        var checkName = /^[a-zA-Z\u4e00-\u9fa5]{4,50}$/;
        return this.optional(element)||(checkName.test(value));
    },"*只允许4-50位中英文！");
    // 密码正则表达式
    $.validator.addMethod("checkPass",function(value,element,params){
        var checkPass = /^\w{6,16}$/;
        return this.optional(element)||(checkPass.test(value));
    },"*只允许6-16位英文字母、数字和下画线！");

    // 异步验证 公司名称是否重复
    $("#companyName").change(function () {
        let companyName = $(this).val();
        $.ajax({
            url:"checkCompany?companyName="+companyName,
            type:"get",
            dataType:"json",
            success:function (ret) {
                if (!ret.status){
                    $("#checkN").html("公司名称重复，请检查");
                }else {
                    $("#checkN").html("");

                }
            }
        })
    })

        $("#confirmPassword").blur(function (){
            var password = $("#password").val();
            var confirmPassword = $("#confirmPassword").val();
            if(password != confirmPassword){
                $("#confirmPassword").val("");
                $("#pwd-msg").html("两次输入密码不一致，请重新输入");
            }else {
                $("#pwd-msg").html("");
            }
        })

        /*form.verify({
            password:[
                /^[\S]{6,12}$/
                ,'密码必须6到12位，且不能出现空格'
            ]
        })*/
</script>
</html>

<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/23
  Time: 11:17 上午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%String path = request.getContextPath()+"/";%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=path%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <title>拟上市公司跟踪培育服务系统</title>
    <!-- 避免IE使用兼容模式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/jquery.contextMenu.min.css" rel="stylesheet"/>
    <link href="css/font-awesome.min.css" rel="stylesheet"/>
    <link href="css/animate.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet"/>
    <link href="css/skins.css" rel="stylesheet"/>
    <link href="ruoyi/css/ry-ui.css?v=4.6.1" rel="stylesheet"/>
</head>
<body class="fixed-sidebar full-height-layout gray-bg" style="overflow: hidden">
<div id="wrapper">

    <!--左侧导航开始-->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="nav-close">
            <i class="fa fa-times-circle"></i>
        </div>
        <a href="index">
            <li class="logo hidden-xs">
                <span class="logo-lg">山西晋信资本</span>
            </li>
        </a>
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">

                <li>
                    <a class="menuItem" href="/system/main"><i class="fa fa-home"></i> <span class="nav-label">首页</span> </a>
                </li>

                <c:forEach items="${menus}" var="menu">
                    <li>
                        <a <c:if test="${empty menu.url && menu.url!='#'}">class="${menu.target}"</c:if>
                           href="${menu.url}"

                           data-refresh="${menu.isRefresh == '0'}">
                            <i class="fa ${menu.icon}" ></i>
                            <span class="nav-label" >${menu.menuName}</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <c:forEach items="${menu.children}" var="cmenu">
                                <li>
                                    <c:if test="${empty cmenu.children}">
                                        <a href="${cmenu.url}" <c:if test="${empty cmenu.target}"> class="menuItem" </c:if><c:if test="${not empty cmenu.target}"> class="${cmenu.target}"</c:if> data-refresh="${cemenu.isRefresh == '0'}">${cmenu.menuName}</a>
                                    </c:if>
                                    <c:if test="${not empty cmenu.children}">
                                        <a href="#">${cmenu.menuName}<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level">
                                            <c:forEach items="${cmenu.children}" var="emenu">
                                                <li>
                                                <c:if test="${empty emenu.children}">
                                                    <a href="${emenu.url}" <c:if test="${empty emenu.target or emenu.target==''}">class="menuItem"</c:if><c:if test="${not empty emenu.target}">class="${emenu.target}"</c:if> data-refresh="${emenu.isRefresh == '0'}">${emenu.menuName}</a>
                                                </c:if>
                                                <c:if test="${not empty emenu.children}">
                                                    <a href="#">${emenu.menuName}<span class="fa arrow"></span></a>
                                                    <ul  class="nav nav-four-level">
                                                        <c:forEach items="${emenu.children}" var="fmenu">
                                                            <li>
                                                                <c:if test="${empty fmenu.children}">
                                                                    <a <c:if test="${empty fmenu.target}">class="menuItem"</c:if><c:if test="${not empty fmenu.target}">class="${fmenu.target}"</c:if>
                                                                       href="${fmenu.url}" data-refresh="${cmenu.isRefresh == '0'}">${fmenu.menuName}</a>
                                                                </c:if>

                                                            </li>
                                                        </c:forEach>
                                                    </ul>
                                                </c:if>
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </c:if>
                                </li>
                            </c:forEach>
                        </ul>
                    </li>
                </c:forEach>

            </ul>
        </div>
    </nav>
    <!--左侧导航结束-->

    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2" style="color:#FFF;" href="#" title="收起菜单">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <div class="navbar-header" style="line-height: 50px;font-size:20px;">
                    <a  style="color:#FFF;" >
                        拟上市公司跟踪培育服务系统
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right welcome-message">
                    <li><a data-toggle="tooltip" data-trigger="hover" data-placement="bottom" title="开发文档" href="http://doc.ruoyi.vip/ruoyi" target="_blank"><i class="fa fa-question-circle"></i> 文档</a></li>
                    <li><a data-toggle="tooltip" data-trigger="hover" data-placement="bottom" title="全屏显示" href="#" id="fullScreen"><i class="fa fa-arrows-alt"></i> 全屏</a></li>
                    <li class="dropdown user-menu">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-hover="dropdown">
                            <c:if test="${empty user.avatar}">
                                <img src="img/profile.jpg" onerror="this.src='img/profile.jpg'" class="user-image">
                            </c:if>
                            <c:if test="${not empty user.avatar}">
                                <img src="${user.avatar}" onerror="this.src='img/profile.jpg'" class="user-image">
                            </c:if>
                            <span class="hidden-xs">${user.userName}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="mt5">
                                <a href="system/user/profile" class="menuItem noactive">
                                    <i class="fa fa-user"></i> 个人中心</a>
                            </li>
                            <li>
                                <a onclick="resetPwd()">
                                    <i class="fa fa-key"></i> 修改密码</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout">
                                    <i class="fa fa-sign-out"></i> 退出登录</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row content-tabs">
            <button class="roll-nav roll-left tabLeft">
                <i class="fa fa-backward"></i>
            </button>
            <nav class="page-tabs menuTabs">
                <div class="page-tabs-content">
                    <a href="javascript:;" class="active menuTab" th:data-id="@{/system/main}">首页</a>
                </div>
            </nav>
            <button class="roll-nav roll-right tabRight">
                <i class="fa fa-forward"></i>
            </button>
            <a href="javascript:void(0);" class="roll-nav roll-right tabReload"><i class="fa fa-refresh"></i> 刷新</a>
        </div>

        <a id="ax_close_max" class="ax_close_max" href="#" title="关闭全屏"> <i class="fa fa-times-circle-o"></i> </a>

                                                          <div class="row mainContent" id="content-main" <c:if test="${not ignoreFooter}">style="height: calc(100% - 91px)"</c:if>>

            <iframe class="RuoYi_iframe" name="iframe0" width="100%" height="100%" th:data-id="@{/system/main}"
                    src="system/main" frameborder="0" seamless></iframe>
        </div>


    </div>
    <!--右侧部分结束-->
</div>
<!-- 全局js -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="js/jquery.contextMenu.min.js"></script>
<script src="ajax/libs/blockUI/jquery.blockUI.js"></script>
<script src="ajax/libs/layer/layer.min.js"></script>
<script src="ruoyi/js/ry-ui.js?v=4.6.1"></script>
<script src="ruoyi/js/common.js?v=4.6.1"></script>
<script src="ruoyi/index.js?v=20201208"></script>
<script src="ajax/libs/fullscreen/jquery.fullscreen.js"></script>
<script th:inline="javascript">
    window.history.forward(1);

    // 皮肤缓存
    var skin = storage.get("skin");
    // history（表示去掉地址的#）否则地址以"#"形式展示
    var mode = "history";
    // 历史访问路径缓存
    var historyPath = storage.get("historyPath");
    // 是否页签与菜单联动
    var isLinkage = true;

    // 本地主题优先，未设置取系统配置
    if($.common.isNotEmpty(skin)){
        $("body").addClass(skin.split('|')[0]);
        $("body").addClass(skin.split('|')[1]);
    } else {
        $("body").addClass([[${sideTheme}]]);
        $("body").addClass([[${skinName}]]);
    }

    /* 用户管理-重置密码 */
    function resetPwd() {
        var url = 'system/user/profile/resetPwd';
        $.modal.open("重置密码", url, '770', '380');
    }

    /* 切换主题 */
    function switchSkin() {
        layer.open({
            type : 2,
            shadeClose : true,
            title : "切换主题",
            area : ["530px", "386px"],
            content : [ctx + "system/switchSkin", 'no']
        })
    }

    /* 切换菜单 */
    function toggleMenu() {
        $.modal.confirm("确认要切换成横向菜单吗？", function() {
            $.get( 'system/menuStyle/topnav', function(result) {
                window.location.reload();
            });
        })
    }

    /** 刷新时访问路径页签 */
    function applyPath(url) {
        $('a[href$="' + decodeURI(url) + '"]').click();
        if (!$('a[href$="' + url + '"]').hasClass("noactive")) {
            $('a[href$="' + url + '"]').parent("li").addClass("selected").parents("li").addClass("active").end().parents("ul").addClass("in");
        }
    }

    $(function() {
        var lockPath = storage.get('lockPath');
        if($.common.equals("history", mode) && window.performance.navigation.type == 1) {
            var url = storage.get('publicPath');
            if ($.common.isNotEmpty(url)) {
                applyPath(url);
            }
        } else if($.common.isNotEmpty(lockPath)) {
            applyPath(lockPath);
            storage.remove('lockPath');
        } else {
            var hash = location.hash;
            if ($.common.isNotEmpty(hash)) {
                var url = hash.substring(1, hash.length);
                applyPath(url);
            } else {
                if($.common.equals("history", mode)) {
                    storage.set('publicPath', "");
                }
            }
        }

        /* 初始密码提示 */
        if([[${isDefaultModifyPwd}]]) {
            layer.confirm("您的密码还是初始密码，请修改密码！", {
                icon: 0,
                title: "安全提示",
                btn: ['确认'	, '取消'],
                offset: ['30%']
            }, function (index) {
                resetPwd();
                layer.close(index);
            });
        }

        /* 过期密码提示 */
        if([[${isPasswordExpired}]]) {
            layer.confirm("您的密码已过期，请尽快修改密码！", {
                icon: 0,
                title: "安全提示",
                btn: ['确认'	, '取消'],
                offset: ['30%']
            }, function (index) {
                resetPwd();
                layer.close(index);
            });
        }
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
</body>
</html>

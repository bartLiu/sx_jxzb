<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/22
  Time: 6:20 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%String path = request.getContextPath()+"/";%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=path%>"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>拟上市公司跟踪培育服务系统</title>
    <meta name="description" content="拟上市公司跟踪培育服务系统">
    <link href="css/bootstrap.min.css"  rel="stylesheet"/>
    <link href="css/font-awesome.min.css" rel="stylesheet"/>
    <link href="css/style.css"  rel="stylesheet"/>
    <link href="css/login.min.css"  rel="stylesheet"/>
    <link href="ruoyi/css/ry-ui.css" rel="stylesheet"/>
    <!-- 360浏览器急速模式 -->
    <meta name="renderer" content="webkit">
    <!-- 避免IE使用兼容模式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="favicon.ico" />
    <style type="text/css">label.error { position:inherit;  }</style>
    <script>
        if(window.top!==window.self){alert('未登录或登录超时。请重新登录');window.top.location=window.location};
    </script>
</head>
<body class="signin">
<div class="signinpanel">
    <div class="row">
        <div class="col-sm-6">
            <div class="signin-info">
                <div class="logopanel m-b">
                    <h1></h1>
                </div>
                <div class="m-b"></div>

            </div>
        </div>
        <div class="col-sm-6">
            <form id="signupForm" autocomplete="off">
                <h2 class="no-margins" style="color:#000000;font-weight: bold;font-size:22px;">拟上市公司跟踪培育服务系统</h2><br>
                <input type="text"     name="username" class="form-control uname"     placeholder="请输入用户名" value="admin"    /><br>
                <input type="password" name="password" class="form-control pword"     placeholder="请输入密码"   value="admin" /><br>
                <div style="float:right;margin-bottom: 10px;"><a href="/toRegister">注册</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">忘记密码</a></div>
                <button class="btn btn-success btn-block" id="btnSubmit" data-loading="正在验证登录，请稍后...">登录</button>
            </form>
        </div>
    </div>
</div>
</body>
<!-- 全局js -->
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<!-- 验证插件 -->
<script src="ajax/libs/validate/jquery.validate.min.js" type="text/javascript"></script>
<script src="ajax/libs/validate/messages_zh.min.js" type="text/javascript"></script>
<script src="ajax/libs/layer/layer.min.js" type="text/javascript"></script>
<script src="ajax/libs/blockUI/jquery.blockUI.js"type="text/javascript"></script>
<script src="ruoyi/js/ry-ui.js" type="text/javascript"></script>
<script>

    $(function() {
        validateKickout();
        validateRule();
    });

    $.validator.setDefaults({
        submitHandler: function() {
            login();
        }
    });

    function login() {
        $.modal.loading($("#btnSubmit").data("loading"));
        var username = $.common.trim($("input[name='username']").val());
        var password = $.common.trim($("input[name='password']").val());
        var rememberMe = $("input[name='rememberme']").is(':checked');
        $.ajax({
            type: "post",
            url:  "login",
            data: {
                "username": username,
                "password": password,
                "rememberMe": rememberMe
            },
            success: function(r) {
                if (r.code == web_status.SUCCESS) {
                    location.href = 'index';
                } else {
                    $.modal.closeLoading();
                    $('.imgcode').click();
                    $(".code").val("");
                    $.modal.msg(r.msg);
                }
            }
        });
    }

    function validateRule() {
        var icon = "<i class='fa fa-times-circle'></i> ";
        $("#signupForm").validate({
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                username: {
                    required: icon + "请输入您的用户名",
                },
                password: {
                    required: icon + "请输入您的密码",
                }
            }
        })
    }

    function validateKickout() {
        if (getParam("kickout") == 1) {
            layer.alert("<font color='red'>您已在别处登录，请您修改密码或重新登录</font>", {
                    icon: 0,
                    title: "系统提示"
                },
                function(index) {
                    //关闭弹窗
                    layer.close(index);
                    if (top != self) {
                        top.location = self.location;
                    } else {
                        var url  =  location.search;
                        if (url) {
                            var oldUrl  = window.location.href;
                            var newUrl  = oldUrl.substring(0,  oldUrl.indexOf('?'));
                            self.location  = newUrl;
                        }
                    }
                });
        }
    }

    function getParam(paramName) {
        var reg = new RegExp("(^|&)" + paramName + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return decodeURI(r[2]);
        return null;
    }
</script>
</html>

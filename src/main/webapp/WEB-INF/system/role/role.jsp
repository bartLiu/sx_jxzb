<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:44 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%String path = request.getContextPath()+"/";%>
<!doctype html>
<html>
<head>
    <base href="<%=path%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>角色管理</title>
    <%@include file="/header.jsp"%>
    <link href="ajax/libs/jquery-layout/jquery.layout-latest.css" rel="stylesheet"/>
    <script src="ajax/libs/jquery-layout/jquery.layout-latest.js"></script>
</head>
<body class="gray-bg">
<div class="container-div">
    <div class="row">
        <div class="col-sm-12 search-collapse">
            <form id="role-form">
                <div class="select-list">
                    <ul>
                        <li>
                            角色名称：<input type="text" name="roleName"/>
                        </li>
                        <li>
                            权限字符：<input type="text" name="roleKey"/>
                        </li>
                        <li>
                            角色状态：<select name="status" >
                            <option value="">所有</option>
                        </select>
                        </li>
                        <li class="select-time">
                            <label>创建时间： </label>
                            <input type="text" class="time-input" id="startTime" placeholder="开始时间" name="params[beginTime]"/>
                            <span>-</span>
                            <input type="text" class="time-input" id="endTime" placeholder="结束时间" name="params[endTime]"/>
                        </li>
                        <li>
                            <a class="btn btn-primary btn-rounded btn-sm" onclick="$.table.search()"><i class="fa fa-search"></i>&nbsp;搜索</a>
                            <a class="btn btn-warning btn-rounded btn-sm" onclick="$.form.reset()"><i class="fa fa-refresh"></i>&nbsp;重置</a>
                        </li>
                    </ul>
                </div>
            </form>
        </div>

        <div class="btn-group-sm" id="toolbar" role="group">
            <a class="btn btn-success" onclick="$.operate.add()" shiro:hasPermission="system:role:add">
                <i class="fa fa-plus"></i> 新增
            </a>
            <a class="btn btn-primary single disabled" onclick="$.operate.edit()" shiro:hasPermission="system:role:edit">
                <i class="fa fa-edit"></i> 修改
            </a>
            <a class="btn btn-danger multiple disabled" onclick="$.operate.removeAll()" shiro:hasPermission="system:role:remove">
                <i class="fa fa-remove"></i> 删除
            </a>
            <a class="btn btn-warning" onclick="$.table.exportExcel()" shiro:hasPermission="system:role:export">
                <i class="fa fa-download"></i> 导出
            </a>
        </div>

        <div class="col-sm-12 select-table table-striped">
            <table id="bootstrap-table"></table>
        </div>
    </div>
</div>
<script>
    var editFlag = false;
    <shiro:hasPermission name="system:role:edit">
        editFlag = true;
    </shiro:hasPermission>
    var removeFlag = false;
    <shiro:hasPermission name="system:role:remove">
        removeFlag = true;
    </shiro:hasPermission>
    var prefix =  "system/role";


    $(function() {
        var options = {
            url: prefix + "/list",
            createUrl: prefix + "/add",
            updateUrl: prefix + "/edit/{id}",
            removeUrl: prefix + "/remove",
            exportUrl: prefix + "/export",
            sortName: "roleSort",
            modalName: "角色",
            columns: [{
                checkbox: true
            },
                {
                    field: 'roleId',
                    title: '角色编号'
                },
                {
                    field: 'roleName',
                    title: '角色名称',
                    sortable: true
                },
                {
                    field: 'roleKey',
                    title: '权限字符',
                    sortable: true
                },
                {
                    field: 'roleSort',
                    title: '显示顺序',
                    sortable: true
                },
                {
                    visible: editFlag == 'hidden' ? false : true,
                    title: '角色状态',
                    align: 'center',
                    formatter: function (value, row, index) {
                        return statusTools(row);
                    }
                },
                {
                    field: 'createTime',
                    title: '创建时间',
                    sortable: true
                },
                {
                    title: '操作',
                    align: 'center',
                    formatter: function(value, row, index) {
                        var actions = [];
                        actions.push('<a class="btn btn-success btn-xs ' + editFlag + '" href="javascript:void(0)" onclick="$.operate.edit(\'' + row.roleId + '\')"><i class="fa fa-edit"></i>编辑</a> ');
                        actions.push('<a class="btn btn-danger btn-xs ' + removeFlag + '" href="javascript:void(0)" onclick="$.operate.remove(\'' + row.roleId + '\')"><i class="fa fa-remove"></i>删除</a> ');
                        var more = [];
                        more.push("<a class='btn btn-default btn-xs " + editFlag + "' href='javascript:void(0)' onclick='authDataScope(" + row.roleId + ")'><i class='fa fa-check-square-o'></i>数据权限</a> ");
                        more.push("<a class='btn btn-default btn-xs " + editFlag + "' href='javascript:void(0)' onclick='authUser(" + row.roleId + ")'><i class='fa fa-user'></i>分配用户</a>");
                        actions.push('<a tabindex="0" class="btn btn-info btn-xs" role="button" data-container="body" data-placement="left" data-toggle="popover" data-html="true" data-trigger="hover" data-content="' + more.join('') + '"><i class="fa fa-chevron-circle-right"></i>更多操作</a>');
                        return actions.join('');
                    }
                }]
        };
        $.table.init(options);
    });

    /* 角色管理-分配数据权限 */
    function authDataScope(roleId) {
        var url = prefix + '/authDataScope/' + roleId;
        $.modal.open("分配数据权限", url);
    }

    /* 角色管理-分配用户 */
    function authUser(roleId) {
        var url = prefix + '/authUser/' + roleId;
        $.modal.openTab("分配用户", url);
    }

    /* 角色状态显示 */
    function statusTools(row) {
        if (row.status == 1) {
            return '<i class=\"fa fa-toggle-off text-info fa-2x\" onclick="enable(\'' + row.roleId + '\')"></i> ';
        } else {
            return '<i class=\"fa fa-toggle-on text-info fa-2x\" onclick="disable(\'' + row.roleId + '\')"></i> ';
        }
    }

    /* 角色管理-停用 */
    function disable(roleId) {
        $.modal.confirm("确认要停用角色吗？", function() {
            $.operate.post(prefix + "/changeStatus", { "roleId": roleId, "status": 1 });
        })
    }

    /* 角色管理启用 */
    function enable(roleId) {
        $.modal.confirm("确认要启用角色吗？", function() {
            $.operate.post(prefix + "/changeStatus", { "roleId": roleId, "status": 0 });
        })
    }
</script>
</body>
</html>

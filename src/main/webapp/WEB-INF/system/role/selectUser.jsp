<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:44 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%String path = request.getContextPath()+"/";%>
<html>
<head>
    <base href="<%=path%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>修改密码</title>
    <%@include file="/header.jsp"%>
</head>

<body class="gray-bg">
<div class="container-div">
    <div class="row">
        <div class="col-sm-12 search-collapse">
            <form id="role-form">
                <input type="hidden" id="roleId" name="roleId" value="${role.roleId}">
                <div class="select-list">
                    <ul>
                        <li>
                            登录名称：<input type="text" name="loginName"/>
                        </li>
                        <li>
                            手机号码：<input type="text" name="phonenumber"/>
                        </li>
                        <li>
                            <a class="btn btn-primary btn-rounded btn-sm" onclick="$.table.search()"><i class="fa fa-search"></i>&nbsp;搜索</a>
                            <a class="btn btn-warning btn-rounded btn-sm" onclick="$.form.reset()"><i class="fa fa-refresh"></i>&nbsp;重置</a>
                        </li>
                    </ul>
                </div>
            </form>
        </div>

        <div class="col-sm-12 select-table table-striped">
            <table id="bootstrap-table"></table>
        </div>
    </div>
</div>
<script th:inline="javascript">
    var datas = ${key};
    var prefix =  "system/role/authUser";

    $(function() {
        var options = {
            url: prefix + "/unallocatedList",
            createUrl: prefix + "/add",
            updateUrl: prefix + "/edit/{id}",
            removeUrl: prefix + "/remove",
            exportUrl: prefix + "/export",
            importUrl: prefix + "/importData",
            importTemplateUrl: prefix + "/importTemplate",
            queryParams: queryParams,
            sortName: "createTime",
            sortOrder: "desc",
            modalName: "用户",
            showSearch: false,
            showRefresh: false,
            showToggle: false,
            showColumns: false,
            clickToSelect: true,
            rememberSelected: true,
            columns: [{
                field: 'state',
                checkbox: true
            },
                {
                    field: 'userId',
                    title: '用户ID',
                    visible: false
                },
                {
                    field: 'loginName',
                    title: '登录名称',
                    sortable: true
                },
                {
                    field: 'userName',
                    title: '用户名称'
                },
                {
                    field: 'email',
                    title: '邮箱'
                },
                {
                    field: 'phonenumber',
                    title: '手机'
                },
                {
                    field: 'status',
                    title: '用户状态',
                    align: 'center',
                    formatter: function (value, row, index) {
                        return $.table.selectDictLabel(datas, value);
                    }
                },
                {
                    field: 'createTime',
                    title: '创建时间',
                    sortable: true
                }]
        };
        $.table.init(options);
    });

    function queryParams(params) {
        var search = $.table.queryParams(params);
        search.roleId = $("#roleId").val();
        return search;
    }

    /* 添加用户-选择用户-提交 */
    function submitHandler() {
        var rows = $.table.selectFirstColumns();
        if (rows.length == 0) {
            $.modal.alertWarning("请至少选择一条记录");
            return;
        }
        var data = { "roleId": $("#roleId").val(), "userIds": rows.join() };
        $.operate.save(prefix + "/selectAll", data);
    }
</script>
</body>
</html>
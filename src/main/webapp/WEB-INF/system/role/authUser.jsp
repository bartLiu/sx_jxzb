<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:44 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%String path = request.getContextPath()+"/";%>
<html>
<head>
    <base href="<%=path%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>修改密码</title>
    <%@include file="/header.jsp"%>
    <link href="ajax/libs/jquery-ztree/3.5/css/metro/zTreeStyle.css" rel="stylesheet"/>
    <script src="ajax/libs/jquery-ztree/3.5/js/jquery.ztree.all-3.5.js"></script>
</head>
<body class="gray-bg">
<div class="container-div">
    <div class="row">
        <div class="col-sm-12 search-collapse">
            <form id="role-form">
                <input type="hidden" id="roleId" name="roleId" value="${role.roleId}">
                <div class="select-list">
                    <ul>
                        <li>
                            登录名称：<input type="text" name="loginName"/>
                        </li>
                        <li>
                            手机号码：<input type="text" name="phonenumber"/>
                        </li>
                        <li>
                            <a class="btn btn-primary btn-rounded btn-sm" onclick="$.table.search()"><i class="fa fa-search"></i>&nbsp;搜索</a>
                            <a class="btn btn-warning btn-rounded btn-sm" onclick="$.form.reset()"><i class="fa fa-refresh"></i>&nbsp;重置</a>
                        </li>
                    </ul>
                </div>
            </form>
        </div>

        <div class="btn-group-sm" id="toolbar" role="group">
            <a class="btn btn-success" onclick="selectUser()" shiro:hasPermission="system:role:add">
                <i class="fa fa-plus"></i> 添加用户
            </a>
            <a class="btn btn-danger multiple disabled" onclick="cancelAuthUserAll()" shiro:hasPermission="system:role:remove">
                <i class="fa fa-remove"></i> 批量取消授权
            </a>
            <a class="btn btn-warning" onclick="closeItem()">
                <i class="fa fa-reply-all"></i> 关闭
            </a>
        </div>

        <div class="col-sm-12 select-table table-striped">
            <table id="bootstrap-table"></table>
        </div>
    </div>
</div>
<th:block th:include="include :: footer" />
<script th:inline="javascript">
    var removeFlag = false;
    <shiro:hasPermission name="system:role:remove">
        removeFlag = true;
    </shiro:hasPermission>
    var datas = ${key};
    alert(datas)
    var prefix = "system/role/authUser";

    $(function() {
        var options = {
            url: prefix + "/allocatedList",
            createUrl: prefix + "/add",
            updateUrl: prefix + "/edit/{id}",
            removeUrl: prefix + "/remove",
            exportUrl: prefix + "/export",
            importUrl: prefix + "/importData",
            importTemplateUrl: prefix + "/importTemplate",
            queryParams: queryParams,
            sortName: "createTime",
            sortOrder: "desc",
            modalName: "用户",
            columns: [{
                checkbox: true
            },
                {
                    field: 'userId',
                    title: '用户ID',
                    visible: false
                },
                {
                    field: 'loginName',
                    title: '登录名称',
                    sortable: true
                },
                {
                    field: 'userName',
                    title: '用户名称'
                },
                {
                    field: 'email',
                    title: '邮箱'
                },
                {
                    field: 'phonenumber',
                    title: '手机'
                },
                {
                    field: 'status',
                    title: '用户状态',
                    align: 'center',
                    formatter: function (value, row, index) {
                        return $.table.selectDictLabel(datas, value);
                    }
                },
                {
                    field: 'createTime',
                    title: '创建时间',
                    sortable: true
                },
                {
                    title: '操作',
                    align: 'center',
                    formatter: function(value, row, index) {
                        var actions = [];
                        actions.push('<a class="btn btn-danger btn-xs ' + removeFlag + '" href="javascript:void(0)" onclick="cancelAuthUser(\'' + row.userId + '\')"><i class="fa fa-remove"></i>取消授权</a> ');
                        return actions.join('');
                    }
                }]
        };
        $.table.init(options);
    });

    function queryParams(params) {
        var search = $.table.queryParams(params);
        search.roleId = $("#roleId").val();
        return search;
    }

    /* 分配用户-选择用户 */
    function selectUser() {
        var url = prefix + '/selectUser/' + $("#roleId").val();
        $.modal.open("选择用户", url);
    }

    /* 分配用户-批量取消授权 */
    function cancelAuthUserAll(userId) {
        var rows = $.table.selectFirstColumns();
        if (rows.length == 0) {
            $.modal.alertWarning("请至少选择一条记录");
            return;
        }
        $.modal.confirm("确认要删除选中的" + rows.length + "条数据吗?", function() {
            var data = { "roleId": $("#roleId").val(), "userIds": rows.join() };
            $.operate.submit(prefix + "/cancelAll", "post", "json", data);
        });
    }

    /* 分配用户-取消授权 */
    function cancelAuthUser(userId) {
        $.modal.confirm("确认要取消该用户角色吗？", function() {
            $.operate.post(prefix + "/cancel", { "roleId": $("#roleId").val(), "userId": userId });
        })
    }
</script>
</body>
<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:44 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%String path = request.getContextPath()+"/";%>
<html>
<head>
    <base href="<%=path%>"/>
    <title>Title</title>
    <%@include file="/header.jsp"%>
</head>
<body class="white-bg">
<div class="wrapper wrapper-content animated fadeInRight ibox-content">
    <form class="form-horizontal m" id="form-dict-edit" >
        <input id="dictId" name="dictId"  type="hidden" value="${dict.dictId}" />
        <div class="form-group">
            <label class="col-sm-3 control-label is-required">字典名称：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="dictName" id="dictName" value="${dict.dictName}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label is-required">字典类型：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="dictType" id="dictType" value="${dict.dictType}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">状态：</label>
            <div class="col-sm-8">
                <c:forEach items="${types}" var="d">
                    <div class="radio-box" >
                        <input type="radio" id="${d.dictCode}" name="status" value="${d.dictValue}" <c:if
                            test="${dict.status==d.dictValue}">checked</c:if>>
                        <label>${d.dictLabel}</label>
                    </div>
                </c:forEach>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">备注：</label>
            <div class="col-sm-8">
                <textarea id="remark" name="remark" class="form-control">${dict.remark}</textarea>
            </div>
        </div>
    </form>
</div>
<th:block th:include="include :: footer" />
<script type="text/javascript">
    var prefix = "system/dict";

    $("#form-dict-edit").validate({
        onkeyup: false,
        rules:{
            dictType:{
                minlength: 5,
                remote: {
                    url: prefix + "/checkDictTypeUnique",
                    type: "post",
                    dataType: "json",
                    data: {
                        dictId : function() {
                            return $("#dictId").val();
                        },
                        dictType : function() {
                            return $.common.trim($("#dictType").val());
                        }
                    },
                    dataFilter: function(data, type) {
                        return $.validate.unique(data);
                    }
                }
            },
        },
        messages: {
            "dictType": {
                remote: "该字典类型已经存在"
            }
        },
        focusCleanup: true
    });

    function submitHandler() {
        if ($.validate.form()) {
            $.operate.save(prefix + "/edit", $('#form-dict-edit').serialize());
        }
    }
</script>
</body>
</html>

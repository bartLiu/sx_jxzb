<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:44 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%String path = request.getContextPath()+"/";%>
<!doctype html>
<html>
<head>
    <base href="<%=path%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>数据字典类型</title>
    <%@include file="/header.jsp"%>
</head>
<body class="gray-bg">
<div class="container-div">
    <div class="row">
        <div class="col-sm-12 search-collapse">
            <form id="type-form">
                <div class="select-list">
                    <ul>
                        <li>
                            字典名称：<input type="text" name="dictName"/>
                        </li>
                        <li>
                            字典类型：<input type="text" name="dictType"/>
                        </li>
                        <li>
                            字典状态：<select name="status">
                            <option value="">所有</option>
                            <c:forEach items="${types}" var="dict">
                                <option value="${dict.dictValue}">${dict.dictLabel}</option>
                            </c:forEach>
                        </select>
                        </li>
                        <li class="select-time">
                            <label>创建时间： </label>
                            <input type="text" class="time-input" id="startTime" placeholder="开始时间" name="params[beginTime]"/>
                            <span>-</span>
                            <input type="text" class="time-input" id="endTime" placeholder="结束时间" name="params[endTime]"/>
                        </li>
                        <li>
                            <a class="btn btn-primary btn-rounded btn-sm" onclick="$.table.search()"><i class="fa fa-search"></i>&nbsp;搜索</a>
                            <a class="btn btn-warning btn-rounded btn-sm" onclick="$.form.reset()"><i class="fa fa-refresh"></i>&nbsp;重置</a>
                        </li>
                    </ul>
                </div>
            </form>
        </div>

        <div class="btn-group-sm" id="toolbar" role="group">
            <a class="btn btn-success" onclick="$.operate.add()" shiro:hasPermission="system:dict:add">
                <i class="fa fa-plus"></i> 新增
            </a>
            <a class="btn btn-primary single disabled" onclick="$.operate.edit()" shiro:hasPermission="system:dict:edit">
                <i class="fa fa-edit"></i> 修改
            </a>
            <a class="btn btn-danger multiple disabled" onclick="$.operate.removeAll()" shiro:hasPermission="system:dict:remove">
                <i class="fa fa-remove"></i> 删除
            </a>
            <a class="btn btn-warning" onclick="$.table.exportExcel()" shiro:hasPermission="system:dict:export">
                <i class="fa fa-download"></i> 导出
            </a>
            <a class="btn btn-danger" onclick="clearCache()" shiro:hasPermission="system:dict:remove">
                <i class="fa fa-refresh"></i> 清理缓存
            </a>
        </div>

        <div class="col-sm-12 select-table table-striped">
            <table id="bootstrap-table"></table>
        </div>
    </div>
</div>
<script >

    var editFlag = false;
    <shiro:hasPermission name="system:dict:edit">
    editFlag = true;
    </shiro:hasPermission>
    var removeFlag = false;
    <shiro:hasPermission name="system:dict:remove">
    editFlag = true;
    </shiro:hasPermission>
    var listFlag = false;
    <shiro:hasPermission name="system:dict:list">
    listFlag = true;
    </shiro:hasPermission>
    var datas = ${keys};
    var prefix = "system/dict";

    $(function() {
        var options = {
            url: prefix + "/list",
            createUrl: prefix + "/add",
            updateUrl: prefix + "/edit/{id}",
            removeUrl: prefix + "/remove",
            exportUrl: prefix + "/export",
            sortName: "dictId",
            sortOrder: "asc",
            modalName: "类型",
            columns: [{
                checkbox: true
            },
                {
                    field: 'dictId',
                    title: '字典主键'
                },
                {
                    field: 'dictName',
                    title: '字典名称',
                    sortable: true
                },
                {
                    field: 'dictType',
                    title: '字典类型',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return '<a href="javascript:void(0)" onclick="detail(\'' + row.dictId + '\')">' + value + '</a>';
                    }
                },
                {
                    field: 'status',
                    title: '状态',
                    align: 'center',
                    formatter: function(value, row, index) {
                        return $.table.selectDictLabel(datas, value);
                    }
                },
                {
                    field: 'remark',
                    title: '备注',
                    formatter: function(value, row, index) {
                        return $.table.tooltip(value);
                    }
                },
                {
                    field: 'createTime',
                    title: '创建时间',
                    sortable: true
                },
                {
                    title: '操作',
                    align: 'center',
                    formatter: function(value, row, index) {
                        var actions = [];
                        actions.push('<a class="btn btn-success btn-xs ' + editFlag + '" href="javascript:void(0)" onclick="$.operate.edit(\'' + row.dictId + '\')"><i class="fa fa-edit"></i>编辑</a> ');
                        actions.push('<a class="btn btn-info btn-xs ' + listFlag + '" href="javascript:void(0)" onclick="detail(\'' + row.dictId + '\')"><i class="fa fa-list-ul"></i>列表</a> ');
                        actions.push('<a class="btn btn-danger btn-xs ' + removeFlag + '" href="javascript:void(0)" onclick="$.operate.remove(\'' + row.dictId + '\')"><i class="fa fa-remove"></i>删除</a>');
                        return actions.join('');
                    }
                }]
        };
        $.table.init(options);
    });

    /*字典列表-详细*/
    function detail(dictId) {
        var url = prefix + '/detail/' + dictId;
        $.modal.openTab("字典数据", url);
    }

    /** 清理字典缓存 */
    function clearCache() {
        $.operate.get(prefix + "/clearCache");
    }
</script>
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:44 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%String path = request.getContextPath()+"/";%>
<!doctype html>
<html>
<head>
    <base href="<%=path%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>数据字典数据</title>
    <%@include file="/header.jsp"%>
        <link href="ajax/libs/select2/select2.min.css" rel="stylesheet"/>
        <link href="ajax/libs/select2/select2-bootstrap.css" rel="stylesheet"/>
        <script src="ajax/libs/select2/select2.min.js"></script>
</head>
<body class="gray-bg">
<div class="container-div">
    <div class="row">
        <div class="col-sm-12 search-collapse">
            <form id="data-form">
                <div class="select-list">
                    <ul>
                        <li>
                            字典名称：<select id="dictType" name="dictType" class="form-control">
                           <option value="">全部</option>
                            <c:forEach items="${dictList}" var="d">
                                <option value="${d.dictType}" <c:if test="${dict.dictType==d.dictType}">selected</c:if>>${d.dictName}</option>
                            </c:forEach>
                        </select>
                        </li>
                        <li>
                            字典标签：<input type="text" name="dictLabel"/>
                        </li>
                        <li>
                            数据状态：<select name="status" >
                            <option value="">所有</option>
                            <c:forEach items="${types}" var="type">
                                <option value="${type.dictValue}">${type.dictLabel}</option>
                            </c:forEach>
                        </select>
                        </li>
                        <li>
                            <a class="btn btn-primary btn-rounded btn-sm" onclick="$.table.search()"><i class="fa fa-search"></i>&nbsp;搜索</a>
                            <a class="btn btn-warning btn-rounded btn-sm" onclick="resetPre()"><i class="fa fa-refresh"></i>&nbsp;重置</a>
                        </li>
                    </ul>
                </div>
            </form>
        </div>

        <div class="btn-group-sm" id="toolbar" role="group">
            <a class="btn btn-success" onclick="add()" shiro:hasPermission="system:dict:add">
                <i class="fa fa-plus"></i> 新增
            </a>
            <a class="btn btn-primary single disabled" onclick="$.operate.edit()" shiro:hasPermission="system:dict:edit">
                <i class="fa fa-edit"></i> 修改
            </a>
            <a class="btn btn-danger multiple disabled" onclick="$.operate.removeAll()" shiro:hasPermission="system:dict:remove">
                <i class="fa fa-remove"></i> 删除
            </a>
            <a class="btn btn-warning" onclick="$.table.exportExcel()" shiro:hasPermission="system:dict:export">
                <i class="fa fa-download"></i> 导出
            </a>
            <a class="btn btn-danger" onclick="closeItem()">
                <i class="fa fa-reply-all"></i> 关闭
            </a>
        </div>

        <div class="col-sm-12 select-table table-striped">
            <table id="bootstrap-table"></table>
        </div>
    </div>
</div>
<script >
    var editFlag = false;
    <shiro:hasPermission name="system:dict:edit">
        editFlag = true;
    </shiro:hasPermission>
    var removeFlag = false;
    <shiro:hasPermission name="system:dict:remove">
        removeFlag = true;
    </shiro:hasPermission>
    var datas = ${keys};
    var prefix = "system/dict/data";

    $(function() {
        var options = {
            url: prefix + "/list",
            createUrl: prefix + "/add/{id}",
            updateUrl: prefix + "/edit/{id}",
            removeUrl: prefix + "/remove",
            exportUrl: prefix + "/export",
            queryParams: queryParams,
            sortName: "dictSort",
            sortOrder: "asc",
            modalName: "数据",
            columns: [{
                checkbox: true
            },
                {
                    field: 'dictCode',
                    title: '字典编码'
                },
                {
                    field: 'dictLabel',
                    title: '字典标签',
                    formatter: function(value, row, index) {
                        var listClass = $.common.equals("default", row.listClass) || $.common.isEmpty(row.listClass) ? "" : "badge badge-" + row.listClass;
                        return $.common.sprintf("<span class='%s'>%s</span>", listClass, value);
                    }
                },
                {
                    field: 'dictValue',
                    title: '字典键值'
                },
                {
                    field: 'dictSort',
                    title: '字典排序'
                },
                {
                    field: 'status',
                    title: '状态',
                    align: 'center',
                    formatter: function(value, row, index) {
                        return $.table.selectDictLabel(datas, value);
                    }
                },
                {
                    field: 'remark',
                    title: '备注'
                },
                {
                    field: 'createTime',
                    title: '创建时间',
                    sortable: true
                },
                {
                    title: '操作',
                    align: 'center',
                    formatter: function(value, row, index) {
                        var actions = [];
                        actions.push('<a class="btn btn-success btn-xs ' + editFlag + '" href="javascript:void(0)" onclick="$.operate.edit(\'' + row.dictCode + '\')"><i class="fa fa-edit"></i>编辑</a> ');
                        actions.push('<a class="btn btn-danger btn-xs ' + removeFlag + '" href="javascript:void(0)" onclick="$.operate.remove(\'' + row.dictCode + '\')"><i class="fa fa-remove"></i>删除</a>');
                        return actions.join('');
                    }
                }]
        };
        $.table.init(options);
    });

    function queryParams(params) {
        var search = $.table.queryParams(params);
        search.dictType = $("#dictType").val();
        return search;
    }

    /*字典数据-新增字典*/
    function add() {
        var dictType = $("#dictType option:selected").val();
        $.operate.add(dictType);
    }

    function resetPre() {
        $.form.reset();
        $("#dictType").val($("#dictType").val()).trigger("change");
    }
</script>
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:44 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%String path = request.getContextPath()+"/";%>
<html>
<head>
    <base href="<%=path%>"/>
    <title>Title</title>
    <%@include file="/header.jsp"%>
</head>
<body class="white-bg">
<div class="wrapper wrapper-content animated fadeInRight ibox-content">
    <form class="form-horizontal m" id="form-dict-edit" >
        <input name="dictCode"  type="hidden"  value="${dict.dictCode}" />
        <div class="form-group">
            <label class="col-sm-3 control-label is-required">字典标签：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="dictLabel" id="dictLabel" value="${dict.dictLabel}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label is-required">字典键值：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="dictValue" id="dictValue" value="${dict.dictValue}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">字典类型：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="dictType" readonly="true" value="${dict.dictType}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">样式属性：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" id="cssClass" name="cssClass" value="${dict.cssClass}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label is-required">字典排序：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="dictSort" value="${dict.dictSort}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">回显样式：</label>
            <div class="col-sm-8">
                <select name="listClass" class="form-control m-b">
                    <option value=""        >---请选择---</option>
                    <option value="default"  <c:if test="${dict.listClass=='default'}">selected</c:if>>默认</option>
                    <option value="primary"  <c:if test="${dict.listClass=='primary'}">selected</c:if>>主要</option>
                    <option value="success"  <c:if test="${dict.listClass=='success'}">selected</c:if>>成功</option>
                    <option value="info"     <c:if test="${dict.listClass=='info'}">selected</c:if>>信息</option>
                    <option value="warning"  <c:if test="${dict.listClass=='warning'}">selected</c:if>>警告</option>
                    <option value="danger"   <c:if test="${dict.listClass=='danger'}">selected</c:if>>危险</option>
                </select>
                <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> table表格字典列显示样式属性</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">系统默认：</label>
            <div class="col-sm-8">
                <c:forEach items="${typeYes}" var="type">
                    <div class="radio-box">
                        <input type="radio" name="isDefault" value="${type.dictValue}" <c:if test="${dict.isDefault==type.dictValue}">checked</c:if> />
                        <label>${type.dictLabel}</label>
                    </div>
                </c:forEach>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">状态：</label>
            <div class="col-sm-8">
                <c:forEach items="${types}" var="type">
                    <div class="radio-box">
                        <input type="radio" name="status" value="${type.dictValue}" <c:if test="${dict.status == type.dictValue}">checked</c:if>/>
                        <label>${type.dictLabel}</label>
                    </div>
                </c:forEach>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">备注：</label>
            <div class="col-sm-8">
                <textarea id="remark" name="remark" class="form-control">${dict.remark}</textarea>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    var prefix = "system/dict/data";

    $("#form-dict-edit").validate({
        rules:{
            dictSort:{
                digits:true
            },
        },
        focusCleanup: true
    });

    function submitHandler() {
        if ($.validate.form()) {
            $.operate.save(prefix + "/edit", $('#form-dict-edit').serialize());
        }
    }
</script>
</body>
</html>

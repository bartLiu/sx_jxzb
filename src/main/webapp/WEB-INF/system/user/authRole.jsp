<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:12 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%String path = request.getContextPath()+"/";%>
<html>
<head>
    <base href="<%=path%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>修改密码</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/font-awesome.min.css" rel="stylesheet"/>
    <!-- bootstrap-table 表格插件样式 -->
    <link href="ajax/libs/bootstrap-table/bootstrap-table.min.css?v=20210202" rel="stylesheet"/>
    <link href="css/animate.css" rel="stylesheet"/>
    <link href="css/style.css?v=20200903" rel="stylesheet"/>
    <link href="ruoyi/css/ry-ui.css?v=4.6.1" rel="stylesheet"/>
    <a id="scroll-up" href="#" class="btn btn-sm display"><i class="fa fa-angle-double-up"></i></a>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- bootstrap-table 表格插件 -->
    <script src="ajax/libs/bootstrap-table/bootstrap-table.min.js?v=20210202"></script>
    <script src="ajax/libs/bootstrap-table/locale/bootstrap-table-zh-CN.min.js?v=20210202"></script>
    <script src="ajax/libs/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js?v=20210202"></script>
    <!-- jquery-validate 表单验证插件 -->
    <script src="ajax/libs/validate/jquery.validate.min.js"></script>
    <script src="ajax/libs/validate/messages_zh.min.js"></script>
    <script src="ajax/libs/validate/jquery.validate.extend.js"></script>
    <!-- jquery-validate 表单树插件 -->
    <script src="ajax/libs/bootstrap-treetable/bootstrap-treetable.js"></script>
    <!-- 遮罩层 -->
    <script src="ajax/libs/blockUI/jquery.blockUI.js"></script>
    <script src="ajax/libs/iCheck/icheck.min.js"></script>
    <script src="ajax/libs/layer/layer.min.js"></script>
    <script src="ajax/libs/layui/layui.js"></script>
    <script src="ruoyi/js/common.js?v=4.6.1"></script>
    <script src="ruoyi/js/ry-ui.js?v=4.6.1"></script>
</head>
<body>
<div class="main-content">
    <form id="form-user-add" class="form-horizontal">
        <input type="hidden" id="userId" name="userId" value="${user.userId}">
        <h4 class="form-header h4">基本信息</h4>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-4 control-label is-required">用户名称：</label>
                    <div class="col-sm-8">
                        <input name="userName" class="form-control" type="text" disabled value="${user.userName}">
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-4 control-label is-required">登录账号：</label>
                    <div class="col-sm-8">
                        <input name="loginName" class="form-control" type="text" disabled value="${user.loginName}">
                    </div>
                </div>
            </div>
        </div>

        <h4 class="form-header h4">分配角色</h4>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12 select-table table-striped">
                    <table id="bootstrap-table"></table>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="row">
    <div class="col-sm-offset-5 col-sm-10">
        <button type="button" class="btn btn-sm btn-primary" onclick="submitHandler()"><i class="fa fa-check"></i>保 存</button>&nbsp;
        <button type="button" class="btn btn-sm btn-danger" onclick="closeItem()"><i class="fa fa-reply-all"></i>关 闭 </button>
    </div>
</div>
<script>
    var prefix =  "system/user/authRole";
    var roles = JSON.parse('${roles}');
    $(function() {
        var options = {
            data: roles,
            sidePagination: "client",
            sortName: "roleSort",
            showSearch: false,
            showRefresh: false,
            showToggle: false,
            showColumns: false,
            clickToSelect: true,
            maintainSelected: true,
            columns: [{
                checkbox: true,
                formatter:function (value, row, index) {
                    if($.common.isEmpty(value)) {
                        return { checked: row.flag };
                    } else {
                        return { checked: value }
                    }
                }
            },
                {
                    field: 'roleId',
                    title: '角色编号'
                },
                {
                    field: 'roleSort',
                    title: '排序',
                    sortable: true,
                    visible: false
                },
                {
                    field: 'roleName',
                    title: '角色名称'
                },
                {
                    field: 'roleKey',
                    title: '权限字符',
                    sortable: true
                },
                {
                    field: 'createTime',
                    title: '创建时间',
                    sortable: true
                }]
        };
        $.table.init(options);
    });

    /* 添加角色-提交 */
    function submitHandler(index, layero){
        var rows = $.table.selectFirstColumns();
        var data = { "userId": $("#userId").val(), "roleIds": rows.join() };
        $.operate.saveTab(prefix + "/insertAuthRole", data);
    }
</script>
</body>
</html>
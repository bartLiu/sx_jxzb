<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/27
  Time: 10:54 上午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%String path = request.getContextPath()+"/";%>
<!DOCTYPE html>
<html >
<head>
    <base href="<%=path%>">
    <%@include file="/header.jsp"%>
    <style type="text/css">.user-info-head{position:relative;display:inline-block;}.user-info-head:hover:after{content:'\f030';position:absolute;left:0;right:0;top:0;bottom:0;color:#eee;background:rgba(0,0,0,0.5);font-family:FontAwesome;font-size:24px;font-style:normal;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;cursor:pointer;line-height:110px;border-radius:50%;}</style>
</head>
<body class="white-bg">
<div class="wrapper wrapper-content animated fadeInRight ibox-content">
    <form class="form-horizontal m" id="form-user-resetPwd">
        <input name="userId"  type="hidden"  th:value="${user.userId}" />
        <div class="form-group">
            <label class="col-sm-3 control-label">登录名称：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" readonly="true" name="loginName" value="${user.loginName}"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">旧密码：</label>
            <div class="col-sm-8">
                <input class="form-control" type="password" name="oldPassword" id="oldPassword">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">新密码：</label>
            <div class="col-sm-8">
                <input class="form-control" type="password" name="newPassword" id="newPassword">
                <c:forEach items="${types}" var="type">
                    <c:if test="${type==1}">
                        <block><i class="fa fa-info-circle" style="color: red;"></i>  密码只能为0-9数字</block>
                    </c:if>
                    <c:if test="${type==2}">
                        <block><i class="fa fa-info-circle" style="color: red;"></i>  密码只能为a-z和A-Z字母</block>
                    </c:if>
                    <c:if test="${type==3}">
                        <block><i class="fa fa-info-circle" style="color: red;"></i>  密码必须包含（字母，数字）</block>
                    </c:if>
                    <c:if test="${type==4}">
                        <block><i class="fa fa-info-circle" style="color: red;"></i>  密码必须包含（字母，数字，特殊字符!@#$%^&*()-=_+）</block>
                    </c:if>
                </c:forEach>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">再次确认：</label>
            <div class="col-sm-8">
                <input class="form-control" type="password" name="confirmPassword" id="confirmPassword">
                <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 请再次输入您的密码</span>
            </div>
        </div>
    </form>
</div>

<script>
    $("#form-user-resetPwd").validate({
        rules:{
            oldPassword:{
                required:true,
                remote: {
                    url: "system/user/profile/checkPassword",
                    type: "get",
                    dataType: "json",
                    data: {
                        password: function() {
                            return $("input[name='oldPassword']").val();
                        }
                    }
                }
            },
            newPassword: {
                required: true,
                minlength: 5,
                maxlength: 20
            },
            confirmPassword: {
                required: true,
                equalTo: "#newPassword"
            }
        },
        messages: {
            oldPassword: {
                required: "请输入原密码",
                remote: "原密码错误"
            },
            newPassword: {
                required: "请输入新密码",
                minlength: "密码不能小于5个字符",
                maxlength: "密码不能大于20个字符"
            },
            confirmPassword: {
                required: "请再次输入新密码",
                equalTo: "两次密码输入不一致"
            }

        },
        focusCleanup: true
    });

    function submitHandler() {
        var chrtype = '${jsonTypes}';
        var password = $("#newPassword").val();
        if ($.validate.form() && checkpwd(chrtype, password)) {
            $.operate.save("system/user/profile/resetPwd", $('#form-user-resetPwd').serialize());
        }
    }
</script>
</body>

</html>

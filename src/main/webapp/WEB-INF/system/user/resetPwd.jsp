<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:12 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%String path = request.getContextPath()+"/";%>
<html>
<head>
    <base href="<%=path%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>修改密码</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/font-awesome.min.css" rel="stylesheet"/>
    <!-- bootstrap-table 表格插件样式 -->
    <link href="ajax/libs/bootstrap-table/bootstrap-table.min.css?v=20210202" rel="stylesheet"/>
    <link href="css/animate.css" rel="stylesheet"/>
    <link href="css/style.css?v=20200903" rel="stylesheet"/>
    <link href="ruoyi/css/ry-ui.css?v=4.6.1" rel="stylesheet"/>
    <a id="scroll-up" href="#" class="btn btn-sm display"><i class="fa fa-angle-double-up"></i></a>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- bootstrap-table 表格插件 -->
    <script src="ajax/libs/bootstrap-table/bootstrap-table.min.js?v=20210202"></script>
    <script src="ajax/libs/bootstrap-table/locale/bootstrap-table-zh-CN.min.js?v=20210202"></script>
    <script src="ajax/libs/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js?v=20210202"></script>
    <!-- jquery-validate 表单验证插件 -->
    <script src="ajax/libs/validate/jquery.validate.min.js"></script>
    <script src="ajax/libs/validate/messages_zh.min.js"></script>
    <script src="ajax/libs/validate/jquery.validate.extend.js"></script>
    <!-- jquery-validate 表单树插件 -->
    <script src="ajax/libs/bootstrap-treetable/bootstrap-treetable.js"></script>
    <!-- 遮罩层 -->
    <script src="ajax/libs/blockUI/jquery.blockUI.js"></script>
    <script src="ajax/libs/iCheck/icheck.min.js"></script>
    <script src="ajax/libs/layer/layer.min.js"></script>
    <script src="ajax/libs/layui/layui.js"></script>
    <script src="ruoyi/js/common.js?v=4.6.1"></script>
    <script src="ruoyi/js/ry-ui.js?v=4.6.1"></script>
</head>
<body class="white-bg">
<div class="wrapper wrapper-content animated fadeInRight ibox-content">
    <form class="form-horizontal m" id="form-user-resetPwd">
        <input name="userId"  type="hidden"  value="${user.userId}" />
        <div class="form-group">
            <label class="col-sm-3 control-label">登录名称：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" readonly="true" name="loginName" value="${user.loginName}"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">输入密码：</label>
            <div class="col-sm-8">
                <input class="form-control" type="password" name="password" id="password" >
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $("#form-user-resetPwd").validate({
        rules:{
            password:{
                required:true,
                minlength: 5,
                maxlength: 20
            },
        },
        focusCleanup: true
    });

    function submitHandler() {
        if ($.validate.form()) {
            $.operate.save("system/user/resetPwd", $('#form-user-resetPwd').serialize());
        }
    }
</script>
</body>

</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 3:19 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%String path = request.getContextPath()+"/";%>
<html>
<head>
    <base href="<%=path%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>编辑用户管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/font-awesome.min.css" rel="stylesheet"/>
    <!-- bootstrap-table 表格插件样式 -->
    <link href="ajax/libs/bootstrap-table/bootstrap-table.min.css?v=20210202" rel="stylesheet"/>
    <link href="css/animate.css" rel="stylesheet"/>
    <link href="css/style.css?v=20200903" rel="stylesheet"/>
    <link href="ruoyi/css/ry-ui.css?v=4.6.1" rel="stylesheet"/>
    <a id="scroll-up" href="#" class="btn btn-sm display"><i class="fa fa-angle-double-up"></i></a>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- bootstrap-table 表格插件 -->
    <script src="ajax/libs/bootstrap-table/bootstrap-table.min.js?v=20210202"></script>
    <script src="ajax/libs/bootstrap-table/locale/bootstrap-table-zh-CN.min.js?v=20210202"></script>
    <script src="ajax/libs/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js?v=20210202"></script>
    <!-- jquery-validate 表单验证插件 -->
    <script src="ajax/libs/validate/jquery.validate.min.js"></script>
    <script src="ajax/libs/validate/messages_zh.min.js"></script>
    <script src="ajax/libs/validate/jquery.validate.extend.js"></script>
    <!-- jquery-validate 表单树插件 -->
    <script src="ajax/libs/bootstrap-treetable/bootstrap-treetable.js"></script>
    <!-- 遮罩层 -->
    <script src="ajax/libs/blockUI/jquery.blockUI.js"></script>
    <script src="ajax/libs/iCheck/icheck.min.js"></script>
    <script src="ajax/libs/layer/layer.min.js"></script>
    <script src="ajax/libs/layui/layui.js"></script>
    <script src="ruoyi/js/common.js?v=4.6.1"></script>
    <script src="ruoyi/js/ry-ui.js?v=4.6.1"></script>

    <link href="ajax/libs/select2/select2.min.css" rel="stylesheet"/>
    <link href="ajax/libs/select2/select2-bootstrap.css" rel="stylesheet"/>
    <script src="/ajax/libs/select2/select2.min.js"></script>
</head>
<body>
<div class="main-content">
    <form class="form-horizontal" id="form-user-edit" >
        <input name="userId"  type="hidden"  value="${user.userId}" id="userId"/>
        <input name="deptId"  type="hidden"  value="${user.deptId}" id="treeId"/>
        <h4 class="form-header h4">基本信息</h4>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-4 control-label is-required">用户名称：</label>
                    <div class="col-sm-8">
                        <input name="userName" placeholder="请输入用户名称" class="form-control" type="text" maxlength="30" value="${user.userName}" required>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-4 control-label">归属部门：</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <input class="form-control" type="text" name="deptName" onclick="selectDeptTree()" id="treeName" value="${user.dept.deptName}">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-4 control-label">手机号码：</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <input name="phonenumber" id="phonenumber" placeholder="请输入手机号码" class="form-control" type="text" maxlength="11" value="${user.phonenumber}">
                            <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-4 control-label">邮箱：</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <input name="email" id="email" class="form-control email" type="text" maxlength="50" placeholder="请输入邮箱" value="${user.email}">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-4 control-label is-required">登录账号：</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="text" name="loginName" readonly="true" value="${user.loginName}"/>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-4 control-label">用户状态：</label>
                    <div class="col-sm-8">
                        <label class="toggle-switch switch-solid">
                            <input type="checkbox" id="status"
                            <c:if test="${user.status=='0'}"> checked="true" </c:if>
                            >
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-4 control-label">岗位：<c:forEach items="${user.postIds}" var="postId">${postId}</c:forEach></label>
                    <div class="col-sm-8">
                        <select id="post" class="form-control select2-multiple" multiple>
                            <c:forEach items="${posts}" var="post">
                                <option value="${post.postId}"
                                        <c:forEach items="${user.posts}" var="postId">
                                            <c:if test="${postId.postId==post.postId}">selected</c:if>
                                        </c:forEach> >${post.postName}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-4 control-label">用户性别：</label>
                    <div class="col-sm-8">
                        <select name="sex" class="form-control m-b">
                            <c:forEach items="${types}" var="dict">
                                <option value="${dict.dictValue}" <c:if test="${user.sex==post.postId}">selected</c:if>>${dict.dictLabel}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-xs-2 control-label">角色：</label>
                    <div class="col-xs-10">
                        <c:forEach items="${roles}" var="role">
                            <label class="check-box">
                            <input name="role" type="checkbox" value="${role.roleId}"
                                <c:forEach items="${user.roles}" var="mrole">
                                    <c:if test="${mrole.roleId==role.roleId}">checked</c:if>

                            </c:forEach>
                                    <c:if test="${role.status=='1'}"> disabled</c:if>">${role.roleName}
                            </label>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
        <h4 class="form-header h4">其他信息</h4>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-xs-2 control-label">备注：</label>
                    <div class="col-xs-10">
                        <textarea name="remark" maxlength="500" class="form-control" rows="3">${user.remark}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="row">
    <div class="col-sm-offset-5 col-sm-10">
        <button type="button" class="btn btn-sm btn-primary" onclick="submitHandler()"><i class="fa fa-check"></i>保 存</button>&nbsp;
        <button type="button" class="btn btn-sm btn-danger" onclick="closeItem()"><i class="fa fa-reply-all"></i>关 闭 </button>
    </div>
</div>
<script src="ajax/libs/select2/select2.min.js"></script>
<script type="text/javascript">
    var prefix =  "system/user";

    $("#form-user-edit").validate({
        onkeyup: false,
        rules:{
            email:{
                email:true,
                remote: {
                    url: prefix + "/checkEmailUnique",
                    type: "post",
                    dataType: "json",
                    data: {
                        "userId": function() {
                            return $("#userId").val();
                        },
                        "email": function() {
                            return $.common.trim($("#email").val());
                        }
                    },
                    dataFilter: function (data, type) {
                        var flag = $.validate.unique(data);
                        return flag;
                    }
                }
            },
            phonenumber:{
                isPhone:true,
                remote: {
                    url: prefix + "/checkPhoneUnique",
                    type: "post",
                    dataType: "json",
                    data: {
                        "userId": function() {
                            return $("#userId").val();
                        },
                        "phonenumber": function() {
                            return $.common.trim($("#phonenumber").val());
                        }
                    },
                    dataFilter: function (data, type) {
                        return $.validate.unique(data);
                    }
                }
            },
        },
        messages: {
            "email": {
                remote: "Email已经存在"
            },
            "phonenumber":{
                remote: "手机号码已经存在"
            }
        },
        focusCleanup: true
    });

    function submitHandler() {
        if ($.validate.form()) {
            var data = $("#form-user-edit").serializeArray();
            var status = $("input[id='status']").is(':checked') == true ? 0 : 1;
            var roleIds = $.form.selectCheckeds("role");
            var postIds = $.form.selectSelects("post");
            data.push({"name": "status", "value": status});
            data.push({"name": "roleIds", "value": roleIds});
            data.push({"name": "postIds", "value": postIds});
            $.operate.saveTab(prefix + "/edit", data);
        }
    }

    /* 用户管理-修改-选择部门树 */
    function selectDeptTree() {
        var deptId = $.common.isEmpty($("#treeId").val()) ? "100" : $("#treeId").val();
        var url = "system/dept/selectDeptTree/" + deptId;
        var options = {
            title: '选择部门',
            width: "380",
            url: url,
            callBack: doSubmit
        };
        $.modal.openOptions(options);
    }

    function doSubmit(index, layero){
        var tree = layero.find("iframe")[0].contentWindow.$._tree;
        var body = layer.getChildFrame('body', index);
        $("#treeId").val(body.find('#treeId').val());
        $("#treeName").val(body.find('#treeName').val());
        layer.close(index);
    }

    $(function() {
        $('#post').select2({
            placeholder: "请选择岗位",
            allowClear: true
        });
    })
</script>
</body>
</html>
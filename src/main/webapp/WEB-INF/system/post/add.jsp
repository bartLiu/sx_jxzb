<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:44 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%String path = request.getContextPath()+"/";%>
<html>
<head>
    <base href="<%=path%>"/>
    <title>Title</title>
    <%@include file="/header.jsp"%>
</head>
<body class="white-bg">
<div class="wrapper wrapper-content animated fadeInRight ibox-content">
    <form class="form-horizontal m" id="form-post-add">
        <div class="form-group">
            <label class="col-sm-3 control-label is-required">岗位名称：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="postName" id="postName" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label is-required">岗位编码：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="postCode" id="postCode" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label is-required">显示顺序：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="postSort" id="postSort" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">岗位状态：</label>
            <div class="col-sm-8">
                <c:forEach items="${keys}" var="dict">
                    <div class="radio-box" >
                        <input type="radio"  name="status" value="${dict.dictValue}" >
                        <label >${dict.dictLabel}</label>
                    </div>
                </c:forEach>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">备注：</label>
            <div class="col-sm-8">
                <textarea id="remark" name="remark" class="form-control"></textarea>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    var prefix = "system/post";

    $("#form-post-add").validate({
        onkeyup: false,
        rules:{
            postName:{
                remote: {
                    url:  "system/post/checkPostNameUnique",
                    type: "post",
                    dataType: "json",
                    data: {
                        "postName" : function() {
                            return $.common.trim($("#postName").val());
                        }
                    },
                    dataFilter: function(data, type) {
                        return $.validate.unique(data);
                    }
                }
            },
            postCode:{
                remote: {
                    url: "system/post/checkPostCodeUnique",
                    type: "post",
                    dataType: "json",
                    data: {
                        "postCode" : function() {
                            return $.common.trim($("#postCode").val());
                        }
                    },
                    dataFilter: function(data, type) {
                        return $.validate.unique(data);
                    }
                }
            },
            postSort:{
                digits:true
            },
        },
        messages: {
            "postCode": {
                remote: "岗位编码已经存在"
            },
            "postName": {
                remote: "岗位名称已经存在"
            }
        },
        focusCleanup: true
    });

    function submitHandler() {
        if ($.validate.form()) {
            $.operate.save(prefix + "/add", $('#form-post-add').serialize());
        }
    }
</script>
</body>
</html>
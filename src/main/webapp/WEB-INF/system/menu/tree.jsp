<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:44 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%String path = request.getContextPath()+"/";%>
<html>
<head>
    <base href="<%=path%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>修改密码</title>
    <%@include file="/header.jsp"%>
    <link href="ajax/libs/jquery-ztree/3.5/css/metro/zTreeStyle.css" rel="stylesheet"/>
    <script src="ajax/libs/jquery-ztree/3.5/js/jquery.ztree.all-3.5.js"></script>
</head>
<style>
    body{height:auto;font-family: "Microsoft YaHei";}
    button{font-family: "SimSun","Helvetica Neue",Helvetica,Arial;}
</style>
<body class="hold-transition box box-main">
<input id="treeId"   name="treeId"    type="hidden" value="${menu.menuId}"/>
<input id="treeName" name="treeName"  type="hidden" value="${menu.menuName}"/>
<div class="wrapper"><div class="treeShowHideButton" onclick="$.tree.toggleSearch();">
    <label id="btnShow" title="显示搜索" style="display:none;">︾</label>
    <label id="btnHide" title="隐藏搜索">︽</label>
</div>
    <div class="treeSearchInput" id="search">
        <label for="keyword">关键字：</label><input type="text" class="empty" id="keyword" maxlength="50">
        <button class="btn" id="btn" onclick="$.tree.searchNode()"> 搜索 </button>
    </div>
    <div class="treeExpandCollapse">
        <a href="#" onclick="$.tree.expand()">展开</a> /
        <a href="#" onclick="$.tree.collapse()">折叠</a>
    </div>
    <div id="tree" class="ztree treeselect"></div>
</div>
<script th:inline="javascript">
    $(function() {
        var url =  "system/menu/menuTreeData";
        var options = {
            url: url,
            expandLevel: 1,
            onClick : zOnClick
        };
        $.tree.init(options);
    });

    function zOnClick(event, treeId, treeNode) {
        var treeId = treeNode.id;
        var treeName = treeNode.name;
        $("#treeId").val(treeId);
        $("#treeName").val(treeName);
    }
</script>
</body>
</html>

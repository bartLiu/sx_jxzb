<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:44 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%String path = request.getContextPath()+"/";%>
<html>
<head>
    <base href="<%=path%>"/>
    <title>Title</title>
    <%@include file="/header.jsp"%>
</head>
<body class="white-bg">
<div class="wrapper wrapper-content animated fadeInRight ibox-content">
    <form class="form-horizontal m" id="form-menu-edit" >
        <input name="menuId"   type="hidden" value="${menu.menuId}"   />
        <input id="treeId" name="parentId" type="hidden" value="${menu.parentId}" />
        <div class="form-group">
            <label class="col-sm-3 control-label">上级菜单：</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <input class="form-control" type="text" onclick="selectMenuTree()" id="treeName" readonly="true" value="${menu.parentName == null ? '无' : menu.parentName}">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label is-required">菜单类型：</label>
            <div class="col-sm-8">
                <label class="radio-box"> <input type="radio"  name="menuType" <c:if test="${menu.menuType=='M'}">checked</c:if> value="M" /> 目录 </label>
                <label class="radio-box"> <input type="radio"  name="menuType" <c:if test="${menu.menuType=='C'}">checked</c:if>  value="C" /> 菜单 </label>
                <label class="radio-box"> <input type="radio"  name="menuType" <c:if test="${menu.menuType=='F'}">checked</c:if>  value="F" /> 按钮 </label>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label is-required">菜单名称：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="menuName" id="menuName" value="${menu.menuName}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">请求地址：</label>
            <div class="col-sm-8">
                <input id="url" name="url" class="form-control" type="text" value="${menu.url}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">打开方式：</label>
            <div class="col-sm-8">
                <select id="target" name="target" class="form-control m-b">
                    <option value="menuItem"  <c:if test="${menu.target=='menuItem'}">checked</c:if>>页签</option>
                    <option value="menuBlank" <c:if test="${menu.target=='menuBlank'}">checked</c:if>>新窗口</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">权限标识：</label>
            <div class="col-sm-8">
                <input id="perms" name="perms" class="form-control" type="text" value="${menu.perms}">
                <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 控制器中定义的权限标识，如：@RequiresPermissions("")</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label is-required">显示排序：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="orderNum" value="${menu.orderNum}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">图标：</label>
            <div class="col-sm-8">
                <input id="icon" name="icon" class="form-control" type="text" placeholder="选择图标" value="${menu.icon}">
                <div class="ms-parent" style="width: 100%;">
                    <div class="icon-drop animated flipInX" style="display: none;max-height:200px;overflow-y:auto">
                        <div data-th-include="system/menu/icon"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">菜单状态：</label>
            <div class="col-sm-3">
                    <c:forEach items="${types}" var="type">
                        <div class="radio-box" >
                            <input type="radio" id="${type.dictCode}" name="visible" value="${type.dictValue}" <c:if test="${menu.visible==type.dictValue}">checked</c:if>>
                            <label text="${type.dictLabel}">${type.dictLabel}</label>
                        </div>
                    </c:forEach>
            </div>
            <label class="col-sm-2 control-label is-refresh" title="打开菜单选项卡是否刷新页面">是否刷新：<i class="fa fa-question-circle-o"></i></label>
            <div class="col-sm-3 is-refresh">
                <div class="radio-box">
                    <input type="radio" id="refresh-no" name="isRefresh" value="1" <c:if test="${menu.isRefresh==1}">checked</c:if>>
                    <label for="refresh-no">否</label>
                </div>
                <div class="radio-box">
                    <input type="radio" id="refresh-yes" name="isRefresh" value="0" <c:if test="${menu.isRefresh==0}">checked</c:if>>
                    <label for="refresh-yes">是</label>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    var prefix = "system/menu";

    $(function() {
        var menuType = $('input[name="menuType"]:checked').val();
        menuVisible(menuType);
    });

    $("#form-menu-edit").validate({
        onkeyup: false,
        rules:{
            menuType:{
                required:true,
            },
            menuName:{
                remote: {
                    url: prefix + "/checkMenuNameUnique",
                    type: "post",
                    dataType: "json",
                    data: {
                        "menuId": function() {
                            return $("#menuId").val();
                        },
                        "parentId": function() {
                            return $("input[name='parentId']").val();
                        },
                        "menuName": function() {
                            return $.common.trim($("#menuName").val());
                        }
                    },
                    dataFilter: function(data, type) {
                        return $.validate.unique(data);
                    }
                }
            },
            orderNum:{
                digits:true
            },
        },
        messages: {
            "menuName": {
                remote: "菜单已经存在"
            }
        },
        focusCleanup: true
    });

    function submitHandler() {
        if ($.validate.form()) {
            $.operate.save(prefix + "/edit", $('#form-menu-edit').serialize());
        }
    }

    $(function() {
        $("input[name='icon']").focus(function() {
            $(".icon-drop").show();
        });
        $("#form-menu-edit").click(function(event) {
            var obj = event.srcElement || event.target;
            if (!$(obj).is("input[name='icon']")) {
                $(".icon-drop").hide();
            }
        });
        $(".icon-drop").find(".ico-list i").on("click",
            function() {
                $('#icon').val($(this).attr('class'));
            });
        $('input').on('ifChecked',
            function(event) {
                var menuType = $(event.target).val();
                menuVisible(menuType);
            });
    });

    function menuVisible(menuType) {
        if (menuType == "M") {
            $("#url").parents(".form-group").hide();
            $("#perms").parents(".form-group").hide();
            $("#icon").parents(".form-group").show();
            $("#target").parents(".form-group").hide();
            $("input[name='visible']").parents(".form-group").show();
            $(".is-refresh").hide();
        } else if (menuType == "C") {
            $("#url").parents(".form-group").show();
            $("#perms").parents(".form-group").show();
            $("#icon").parents(".form-group").show();
            $("#target").parents(".form-group").show();
            $("input[name='visible']").parents(".form-group").show();
            $(".is-refresh").show();
        } else if (menuType == "F") {
            $("#url").parents(".form-group").hide();
            $("#perms").parents(".form-group").show();
            $("#icon").parents(".form-group").hide();
            $("#target").parents(".form-group").hide();
            $("input[name='visible']").parents(".form-group").hide();
            $(".is-refresh").hide();
        }
    }

    /*菜单管理-修改-选择菜单树*/
    function selectMenuTree() {
        var menuId = $("#treeId").val();
        if(menuId > 0) {
            var url = prefix + "/selectMenuTree/" + menuId;
            $.modal.open("选择菜单", url, '380', '380');
        } else {
            $.modal.alertError("主菜单不能选择");
        }
    }

    function selectMenuTree() {
        var menuId = $("#treeId").val();
        if(menuId > 0) {
            var url = prefix + "/selectMenuTree/" + menuId;
            var options = {
                title: '菜单选择',
                width: "380",
                url: url,
                callBack: doSubmit
            };
            $.modal.openOptions(options);
        } else {
            $.modal.alertError("主菜单不能选择");
        }
    }

    function doSubmit(index, layero){
        var body = layer.getChildFrame('body', index);
        $("#treeId").val(body.find('#treeId').val());
        $("#treeName").val(body.find('#treeName').val());
        layer.close(index);
    }
</script>
</body>
</html>
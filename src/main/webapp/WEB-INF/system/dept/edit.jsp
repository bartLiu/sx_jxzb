<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:44 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%String path = request.getContextPath()+"/";%>
<html>
<head>
    <base href="<%=path%>"/>
    <title>Title</title>
    <%@include file="/header.jsp"%>
</head>
<body class="white-bg">
<div class="wrapper wrapper-content animated fadeInRight ibox-content">
    <form class="form-horizontal m" id="form-dept-edit" >
        <input name="deptId" id="deptId" type="hidden" value="${dept.deptId}" />
        <input id="treeId" name="parentId" type="hidden" value="${dept.parentId}" />
        <div class="form-group">
            <label class="col-sm-3 control-label">上级部门：</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <input class="form-control" type="text" id="treeName" onclick="selectDeptTree()" readonly="true" value="${dept.parentName}">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label is-required">部门名称：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="deptName" value="${dept.deptName}" id="deptName" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label is-required">显示排序：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="orderNum" value="${dept.orderNum}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">负责人：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="leader" value="${dept.leader}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">联系电话：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="phone" value="${dept.phone}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">邮箱：</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="email" value="${dept.email}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">部门状态：</label>
            <div class="col-sm-8">

                <c:forEach items="${keys}" var="dict">
                    <div class="radio-box" >
                        <input type="radio"  name="status" <c:if test="${dict.dictValue==dept.status}">checked</c:if> value="${dict.dictValue}" >
                        <label >${dict.dictLabel}</label>
                    </div>
                </c:forEach>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    var prefix =  "system/dept";

    $("#form-dept-edit").validate({
        onkeyup: false,
        rules:{
            deptName:{
                remote: {
                    url: prefix + "/checkDeptNameUnique",
                    type: "post",
                    dataType: "json",
                    data: {
                        "deptId": function() {
                            return $("#deptId").val();
                        },
                        "parentId": function() {
                            return $("input[name='parentId']").val();
                        },
                        "deptName": function() {
                            return $.common.trim($("#deptName").val());
                        }
                    },
                    dataFilter: function(data, type) {
                        return $.validate.unique(data);
                    }
                }
            },
            orderNum:{
                digits:true
            },
            email:{
                email:true,
            },
            phone:{
                isPhone:true,
            },
        },
        messages: {
            "deptName": {
                remote: "部门已经存在"
            }
        },
        focusCleanup: true
    });

    function submitHandler() {
        if ($.validate.form()) {
            $.operate.save(prefix + "/edit", $('#form-dept-edit').serialize());
        }
    }

    /*部门管理-修改-选择部门树*/
    function selectDeptTree() {
        var deptId = $("#treeId").val();
        var excludeId = $("input[name='deptId']").val();
        if(deptId > 0) {
            var options = {
                title: '部门选择',
                width: "380",
                url: prefix + "/selectDeptTree/" + $("#treeId").val() + "/" + excludeId,
                callBack: doSubmit
            };
            $.modal.openOptions(options);
        } else {
            $.modal.alertError("父部门不能选择");
        }
    }

    function doSubmit(index, layero){
        var tree = layero.find("iframe")[0].contentWindow.$._tree;
        if ($.tree.notAllowLastLevel(tree)) {
            var body = layer.getChildFrame('body', index);
            $("#treeId").val(body.find('#treeId').val());
            $("#treeName").val(body.find('#treeName').val());
            layer.close(index);
        }
    }
</script>
</body>
</html>
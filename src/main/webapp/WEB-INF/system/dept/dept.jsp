<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:44 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%String path = request.getContextPath()+"/";%>
<!doctype html>
<html>
<head>
    <base href="<%=path%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>部门管理</title>
    <%@include file="/header.jsp"%>
</head>
<body class="gray-bg">
<div class="container-div">
    <div class="row">
        <div class="col-sm-12 search-collapse">
            <form id="dept-form">
                <div class="select-list">
                    <ul>
                        <li>
                            部门名称：<input type="text" name="deptName"/>
                        </li>
                        <li>
                            部门状态：<select name="status" >
                            <option value="">所有</option>
                            <c:forEach items="${keys}" var="key">
                                <option value="${key.dictValue}">${key.dictLabel}</option>
                            </c:forEach>
                        </select>
                        </li>
                        <li>
                            <a class="btn btn-primary btn-rounded btn-sm" onclick="$.treeTable.search()"><i class="fa fa-search"></i>&nbsp;搜索</a>
                            <a class="btn btn-warning btn-rounded btn-sm" onclick="$.form.reset()"><i class="fa fa-refresh"></i>&nbsp;重置</a>
                        </li>
                    </ul>
                </div>
            </form>
        </div>

        <div class="btn-group-sm" id="toolbar" role="group">
            <a class="btn btn-success" onclick="$.operate.add(100)" shiro:hasPermission="system:dept:add">
                <i class="fa fa-plus"></i> 新增
            </a>
            <a class="btn btn-primary" onclick="$.operate.edit()" shiro:hasPermission="system:dept:edit">
                <i class="fa fa-edit"></i> 修改
            </a>
            <a class="btn btn-info" id="expandAllBtn">
                <i class="fa fa-exchange"></i> 展开/折叠
            </a>
        </div>
        <div class="col-sm-12 select-table table-striped">
            <table id="bootstrap-tree-table"></table>
        </div>
    </div>
</div>
<th:block th:include="include :: footer" />
<script th:inline="javascript">
    var addFlag = false;
    <shiro:hasPermission name="system:dept:add">
        addFlag = true;
    </shiro:hasPermission>
    var editFlag = false;
    <shiro:hasPermission name="system:dept:edit">
        editFlag = true;
    </shiro:hasPermission>
    var removeFlag = false;
    <shiro:hasPermission name="system:dept:remove">
        removeFlag = true;
    </shiro:hasPermission>
    var datas = ${types};
    var prefix = "system/dept"

    $(function() {
        var options = {
            code: "deptId",
            parentCode: "parentId",
            uniqueId: "deptId",
            url: prefix + "/list",
            createUrl: prefix + "/add/{id}",
            updateUrl: prefix + "/edit/{id}",
            removeUrl: prefix + "/remove/{id}",
            modalName: "部门",
            columns: [{
                field: 'selectItem',
                radio: true
            },
                {
                    field: 'deptName',
                    title: '部门名称',
                    align: "left"
                },
                {
                    field: 'orderNum',
                    title: '排序',
                    align: "left"
                },
                {
                    field: 'status',
                    title: '状态',
                    align: "left",
                    formatter: function(value, item, index) {
                        return $.table.selectDictLabel(datas, item.status);
                    }
                },
                {
                    field: 'createTime',
                    title: '创建时间',
                    align: "left"
                },
                {
                    title: '操作',
                    align: 'left',
                    formatter: function(value, row, index) {
                        if (row.parentId != 0) {
                            var actions = [];
                            actions.push('<a class="btn btn-success btn-xs ' + editFlag + '" href="javascript:void(0)" onclick="$.operate.edit(\'' + row.deptId + '\')"><i class="fa fa-edit"></i>编辑</a> ');
                            actions.push('<a class="btn btn-info  btn-xs ' + addFlag + '" href="javascript:void(0)" onclick="$.operate.add(\'' + row.deptId + '\')"><i class="fa fa-plus"></i>新增</a> ');
                            actions.push('<a class="btn btn-danger btn-xs ' + removeFlag + '" href="javascript:void(0)" onclick="$.operate.remove(\'' + row.deptId + '\')"><i class="fa fa-trash"></i>删除</a>');
                            return actions.join('');
                        } else {
                            return "";
                        }
                    }
                }]
        };
        $.treeTable.init(options);
    });
</script>
</body>
</html>
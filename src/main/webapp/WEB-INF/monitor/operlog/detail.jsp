<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 2021/4/24
  Time: 10:44 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%String path = request.getContextPath()+"/";%>
<!doctype html>
<html>
<head>
    <base href="<%=path%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>操作日志</title>
    <%@include file="/header.jsp"%>
        <link href="ajax/libs/jsonview/jquery.jsonview.css" rel="stylesheet"/>
        <script src="ajax/libs/jsonview/jquery.jsonview.js"></script>
</head>
<body class="white-bg">
<div class="wrapper wrapper-content animated fadeInRight ibox-content">
    <form class="form-horizontal m-t" id="signupForm">
        <div class="form-group">
            <label class="col-sm-2 control-label">操作模块：</label>
            <div class="form-control-static" >
                ${operLog.title}${label}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">登录信息：</label>
            <div class="form-control-static" >
                ${operLog.operName}/${operLog.deptName}/${operLog.operIp}/${operLog.operLocation}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">请求地址：</label>
            <div class="form-control-static">${operLog.operUrl}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">请求方式：</label>
            <div class="form-control-static" >${operLog.requestMethod}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">操作方法：</label>
            <div class="form-control-static">${operLog.method}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">请求参数：</label>
            <div class="form-control-static"><pre id="operParam"></pre></div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">返回参数：</label>
            <div class="form-control-static"><pre id="jsonResult"></pre></div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">状态：</label>
            <div class="form-control-static" class="${operLog.status == 0 ? 'label label-primary' : 'label label-danger'}">
                <c:if test="${operLog.status == 0}">
                    正常
                </c:if>
                <c:if test="${operLog.status!= 0}">
                    异常
                </c:if>
            </div>
        </div>
        <div class="form-group" style="'display:${operLog.status == 0 ? 'none' : 'block'}">
            <label class="col-sm-2 control-label">异常信息：</label>
            <div class="form-control-static" >${operLog.errorMsg}
            </div>
        </div>
    </form>
</div>
<script >
    $(function() {
        var operParam = '${operLog.operParam}';
        if ($.common.isNotEmpty(operParam) && operParam.length < 2000) {
            $("#operParam").JSONView(operParam);
        } else {
            $("#operParam").text(operParam);
        }
        var jsonResult = '${operLog.jsonResult}';
        if ($.common.isNotEmpty(jsonResult) && jsonResult.length < 2000) {
            $("#jsonResult").JSONView(jsonResult);
        } else {
            $("#jsonResult").text(jsonResult);
        }
    });
</script>
</body>
</html>